extern crate bincode;
extern crate byteorder;
extern crate crypto as rust_crypto;
extern crate futures;
extern crate futures_cpupool;
#[macro_use]
extern crate log;
extern crate r2d2;
extern crate r2d2_redis;
extern crate redis;
extern crate rand;
//serde
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate toml;
//tokio
extern crate tokio_core;
extern crate tokio_proto;
extern crate tokio_service;
extern crate tokio_uds_proto;
extern crate uuid;

#[macro_use]
#[cfg(test)]
extern crate lazy_static;

#[macro_use]
mod macros;
#[macro_use]
pub mod path;
//Modules
mod crypto;
mod networking;
pub mod message;
//mod ui;
pub mod state;
mod handler;
pub mod atom;
pub mod err;
pub mod manifest;
mod permissions;
pub mod app;


pub use self::networking::reqes;
