use err::*;
use manifest::RedisManifest;
use r2d2;
use r2d2_redis::RedisConnectionManager;
use redis;
use serde_json;
use state::Record;
use std::process::{Child, Command, Stdio};

pub struct RedisServer {
    pub child: Child,
}

impl RedisServer {
    pub fn new(config: &RedisManifest) -> RedisServer {
        let rsp: Child = Command::new("redis-server")
            .arg(config.config.clone())
            .stdout(Stdio::null())
            .stderr(Stdio::null())
            .spawn()
            .expect("error starting redis server process");

        RedisServer { child: rsp }
    }
}

/// redis database connection pool
type RedisDBPool = r2d2::Pool<RedisConnectionManager>;
/// connection from redis database connection pool
type RedisPooledConnection = r2d2::PooledConnection<RedisConnectionManager>;

#[derive(Clone)]
pub struct RedisClientPool {
    db_pool: RedisDBPool,
}

impl RedisClientPool {
    pub fn new(config: &RedisManifest) -> Result<RedisClientPool> {
        let db_socket: String = format!("redis+unix://localhost{}?db={}", config.socket, config.db);
        let db_config = r2d2::Config::default();
        let db_manager = RedisConnectionManager::new(db_socket.as_ref())
            .map_err(Error::from)?;
        let db_pool = r2d2::Pool::new(db_config, db_manager)
            .map_err(Error::from)?;

        Ok(RedisClientPool { db_pool: db_pool })
    }

    pub fn get_con(&self) -> Result<RedisClient> {
        let con = self.db_pool.get().map_err(eif!(Error::from))?;
        Ok(RedisClient { client: con })
    }
}

pub struct RedisClient {
    client: RedisPooledConnection,
}

impl RedisClient {
    pub fn create_if_undefined(&self, key: &str, rec: &Record) -> Result<Option<Record>> {
        let status = self.exists(key)?;
        if !status {
            self.set(key, rec)?;
            Ok(Some(self.get(key)?))
        } else {
            Ok(None)
        }
    }
    pub fn get(&self, key: &str) -> Result<Record> {
        let res = redis::cmd("Get")
            .arg(key)
            .query::<String>(&*self.client)
            .map_err(|e| {
                         Error::new(ErrKind::State,
                                    format!("error getting key {} - {}", key, Error::from(e)))
                     });
        Ok(serde_json::from_str(&res?)
               .map_err(|e| {
                            Error::new(ErrKind::State,
                                       format!("error getting key {} - {}", key, Error::from(e)))
                        })?)
    }
    pub fn set(&self, key: &str, rec: &Record) -> Result<()> {
        let json: String = serde_json::to_string(&rec).map_err(Error::from)?;
        let _: () = redis::cmd("SET")
            .arg(key)
            .arg(json)
            .query(&*self.client)
            .map_err(Error::from)?;
        Ok(())
    }
    pub fn exists(&self, key: &str) -> Result<bool> {
        redis::cmd("EXISTS")
            .arg(key)
            .query::<bool>(&*self.client)
            .map_err(Error::from)
    }
    pub fn delete(&self, key: &str) -> Result<bool> {
        redis::cmd("DEL")
            .arg(key)
            .query::<bool>(&*self.client)
            .map_err(Error::from)
    }
    pub fn flush(&self) -> Result<String> {
        redis::cmd("FLUSHDB")
            .query::<String>(&*self.client)
            .map_err(Error::from)
    }
    pub fn save(&self) -> Result<String> {
        redis::cmd("SAVE")
            .query::<String>(&*self.client)
            .map_err(Error::from)
    }
}
