use err::*;
use serde::Deserialize;
use serde_json;
use std::collections::HashSet;

/// Struct for storing data in redis.  data is stored as json Value
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Record {
    pub parent: String,
    pub name: String,
    pub data: String,
    pub children: HashSet<String>,
}
impl Record {
    pub fn get_name(key: String) -> String {
        key.split('.').last().unwrap().into()
    }
    pub fn key_info(key: String) -> Result<(String, String)> {
        let mut iter = key.rsplitn(2, '.');
        let name: String = iter.next().unwrap_or(state_root!()).into();
        let parent: String = iter.next().unwrap_or(state_root!()).into();
        if parent == name {
            Err(Error::new(ErrKind::State, concat!("Reserved Name: {}", state_root!())))
        } else {
            Ok((parent, name))
        }
    }
    pub fn new(parent: String, name: String, data: String) -> Record {
        Record {
            parent: parent,
            name: name,
            data: data,
            children: HashSet::new(),
        }
    }
    pub fn get_children(&self) -> HashSet<String> {
        let mut set: HashSet<String> = HashSet::new();
        for s in &self.children {
            set.insert(Record::get_name(s.clone()));
        }
        set
    }
    pub fn add_child(&mut self, child: String) {
        self.children.insert(child);
    }
    pub fn remove_child(&mut self, child: String) {
        self.children.remove(&child);
    }
    pub fn get_data<T>(&self) -> Result<T>
        where T: Deserialize
    {
        serde_json::from_str::<T>(&self.data).map_err(Error::from)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Transaction {
    True,
    False,
    Item(Record),
    List(Vec<Transaction>),
}
impl Transaction {
    pub fn first(self) -> Option<Record> {
        match self {
            Transaction::True | Transaction::False => None,
            Transaction::Item(r) => Some(r),
            Transaction::List(mut r) => r.remove(0).first(),
        }
    }
    pub fn has_record(&self) -> bool {
        match self {
            &Transaction::False => false,
            &Transaction::True => false,
            &Transaction::Item(_) => true,
            &Transaction::List(ref r) => {
                for r in r {
                    if r.has_record() {
                        return true;
                    }
                }
                return false;
            }
        }
    }
    pub fn is_truthy(&self) -> bool {
        match self {
            &Transaction::False => false,
            &Transaction::True => true,
            &Transaction::Item(_) => true,
            &Transaction::List(ref r) => {
                for r in r {
                    if r.is_truthy() {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}

impl From<Record> for Transaction {
    fn from(item: Record) -> Self {
        Transaction::Item(item)
    }
}
impl From<Vec<Transaction>> for Transaction {
    fn from(list: Vec<Transaction>) -> Self {
        Transaction::List(list)
    }
}

impl From<Vec<Record>> for Transaction {
    fn from(list: Vec<Record>) -> Self {
        let mut v: Vec<Transaction> = Vec::new();
        for r in list {
            v.push(r.into())
        }
        v.into()
    }
}

impl From<Transaction> for Vec<Record> {
    fn from(transaction: Transaction) -> Self {
        let mut v: Vec<Record> = Vec::new();
        match transaction {
            Transaction::Item(r) => v.push(r),
            Transaction::List(r) => {
                for t in r {
                    v.append(&mut t.into())
                }
            }
            Transaction::True | Transaction::False => (),
        }
        v
    }
}