use err::*;
use serde::{Deserialize, Serialize};
use serde_json;

/// CRUD data action to perform on the `State`
#[derive(Debug, Serialize, Deserialize)]
pub enum StateAction {
    Create(String, String),
    Read(String),
    Update(String, String),
    Delete(String),
    Transaction(Vec<StateAction>),
    Conditional(String, Operator, Option<Box<StateAction>>),
}

#[derive(Debug, Serialize, Deserialize)]
pub enum Operator {
    Exists,
    Not(Box<Operator>),
    Intersection(String)
}

/// Convenience functions for making a `StateAction`
impl StateAction {
    pub fn create<S, V>(path: S, value: &V) -> Result<StateAction>
        where S: Into<String>,
              V: Serialize + Deserialize
    {
        let data: Result<String> = serde_json::to_string(value).map_err(eif!(Error::from));
        Ok(StateAction::Create(path.into(), data?))
    }
    pub fn read<S>(path: S) -> StateAction
        where S: Into<String>
    {
        StateAction::Read(path.into())
    }
    pub fn update<S, V>(path: S, value: &V) -> Result<StateAction>
        where S: Into<String>,
              V: Serialize
    {
        let data: Result<String> = serde_json::to_string(value).map_err(eif!(Error::from));
        Ok(StateAction::Update(path.into(), data?))
    }
    pub fn delete<S>(path: S) -> StateAction
        where S: Into<String>
    {
        StateAction::Delete(path.into())
    }
    pub fn conditional<S>(path: S, op: Operator, trans: Option<StateAction>) -> StateAction
        where S: Into<String>
    {
        StateAction::Conditional(path.into(), op, trans.map(|a| Box::new(a)))
    }
}
impl From<Vec<StateAction>> for StateAction {
    fn from(t: Vec<StateAction>) -> Self {
        StateAction::Transaction(t)
    }
}
