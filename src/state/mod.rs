//! This module defines a Request -> Message `State` API and helpful conversion utilities.
//! read and write changes to the state
pub mod redis;
pub mod action;
pub mod record;

use self::action::*;
use self::record::*;
use self::redis::*;
use err::*;
use futures::{BoxFuture, Future};
use futures_cpupool::CpuPool;
use std::collections::HashSet;
use std::fmt;

/// Future returned by a state.injest call
pub type BStateFuture = Box<Future<Item = Transaction, Error = Error>>;
pub type StateFuture = BoxFuture<Transaction, Error>;
/// Intermediate State Operation Result Type
type StateOpResult = Result<Transaction>;

/// Convenience Functions for simple Database Operations

/// Struct containing functions to mutate Redis Database
/// Contains Connection Pool and Thread Pool for handling calls asynchronously
/// Call the injest method to return a future
pub struct State {
    cpu_pool: CpuPool,
    db_pool: RedisClientPool,
}
impl fmt::Debug for State {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "State: {{")?;
        let mut list: Vec<(u8, String)> = Vec::new();

        let con: RedisClient = self.db_pool.get_con().unwrap();
        list.push((1, "root".into()));
        while list.len() > 0 {
            let (depth, key) = list.pop().unwrap();
            let rec: Record = con.get(&key).unwrap();
            for _ in 0..depth {
                write!(f, "    ")?;
            }
            for c in rec.children {
                list.push((depth + 1, c.clone()));
            }
            writeln!(f, "{}: {:?}", rec.name, rec.data)?;
        }
        writeln!(f, "}}")
    }
}

impl State {
    pub fn root_record() -> Record {
        Record::new(state_root!().into(), state_root!().into(), String::new())
    }
    /// Creates a new `State` struct with the given name in its root `Record`.
    pub fn new(rcp: RedisClientPool) -> State {
        // Create a version storage area if it does not already exist
        let state = State {
            cpu_pool: CpuPool::new(10),
            db_pool: rcp,
        };

        state
    }

    /// processes a `StateAction` and returns the result of the operation as a `StateResult`
    pub fn injest<S>(&self, change: S) -> StateFuture
        where S: Into<StateAction>
    {
        let db = self.db_pool.clone();
        let change: StateAction = change.into();
        self.cpu_pool
            .spawn_fn(move || State::op(&db, change))
            .boxed()
    }

    pub fn injest_sync<S>(&self, change: S) -> StateOpResult
        where S: Into<StateAction>
    {
        let change: StateAction = change.into();
        State::op(&self.db_pool, change)
    }

    fn op(db: &RedisClientPool, change: StateAction) -> StateOpResult {
        match change {
            StateAction::Transaction(list) => {
                let mut result: Vec<Transaction> = Vec::new();
                for t in list {
                    result.push(Self::op(db, t)?);
                }
                Ok(result.into())
            }
            StateAction::Create(path, data) => Self::create(&db, path, data),
            StateAction::Read(path) => Self::read(&db, path),
            StateAction::Update(path, data) => Self::update(&db, path, data),
            StateAction::Delete(path) => Self::delete(&db, path),
            StateAction::Conditional(path, op, list) => Self::conditional(&db, path, op, list),
        }
    }
    fn conditional(db: &RedisClientPool,
                   path: String,
                   op: Operator,
                   list: Option<Box<StateAction>>)
                   -> StateOpResult {
        let cond = Self::conditional_op(db, path, op)?;
        match cond {
            true => {
                match list {
                    Some(list) => Self::op(db, *list),
                    None => Ok(Transaction::True),
                }
            }
            false => Ok(Transaction::False),
        }
    }
    fn conditional_op(db: &RedisClientPool, path: String, op: Operator) -> Result<bool> {
        let con: RedisClient = db.get_con()?;
        match op {
            Operator::Exists => con.exists(&path),
            Operator::Not(op) => Self::conditional_op(db, path, *op).map(|val| !val),
            Operator::Intersection(key) => {
                let a: HashSet<String> = Self::read(db, path)?.first().unwrap().get_children();
                let b: HashSet<String> = Self::read(db, key)?.first().unwrap().get_children();
                match a.intersection(&b).next() {
                    Some(_) => Ok(true),
                    None => Ok(false),
                }
            }
        }
    }
    fn create(db: &RedisClientPool, path: String, data: String) -> StateOpResult {
        debug!("create key {:?}", path);
        let con: RedisClient = db.get_con()?;

        match con.exists(&path)? {
            true => Err(Error::new(ErrKind::State, "Key already exists")),
            false => {
                let (key_parent, key_name) = Record::key_info(path.clone())?;
                //Update Parent to include reference
                let mut parent = con.get(&key_parent)?;
                parent.add_child(path.clone());
                con.set(&key_parent, &parent)?;
                //Create Child
                let rec = Record::new(key_parent, key_name, data);
                con.set(&path, &rec)?;

                // [drg] set version here
                con.set(&format!("versioning.latest.created.{}", path), &rec)?;

                con.get(&path).map(|r| r.into())
            }
        }
    }

    fn read(db: &RedisClientPool, path: String) -> StateOpResult {
        debug!("read key {:?}", path);
        let con: RedisClient = db.get_con()?;

        con.get(&path).map(|r| r.into())
    }

    fn update(db: &RedisClientPool, path: String, data: String) -> StateOpResult {
        debug!("update key {:?}", path);
        let con: RedisClient = db.get_con()?;

        Record::key_info(path.clone())?;

        let mut rec: Record = con.get(&path)?;
        rec.data = data;
        con.set(&path, &rec)?;

        // [drg] set version here
        con.set(&format!("versioning.latest.updates.{}", path), &rec)?;

        con.get(&path).map(|r| r.into())
    }

    fn delete(db: &RedisClientPool, path: String) -> StateOpResult {
        debug!("delete key {:?}", path);
        let con: RedisClient = db.get_con()?;

        let (key_parent, _) = Record::key_info(path.clone())?;
        //Update Parent to remove reference
        let mut parent: Record = con.get(&key_parent)?;
        parent.remove_child(path.clone());
        con.set(&key_parent, &parent)?;

        //Recursively remove children
        let mut children: Vec<String> = Vec::new();
        let rec: Record = con.get(&path)?;
        children.push(path);
        while children.len() > 0 {
            let child = children.pop().expect("out of bounds error");
            let child_rec: Record = con.get(&child)?;
            con.delete(&child)?;

            // [drg] add the path to the deleted section of versioning
            con.set(&format!("versioning.latest.deleted.{}", &child), &child_rec)?;
            for ch in child_rec.children {
                children.push(ch);
            }
        }
        Ok(rec.into())
    }
}
