use super::update;
use app::{Application, socket_client, socket_server};
use err::*;
use futures::{BoxFuture, Future, Stream, future};
use manifest::*;
use message::*;
use reqes::*;
use state::BStateFuture;
use state::State;
use state::record::Transaction;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::process::Command;
use std::sync::{Arc, Mutex};
use tokio_core::reactor::Handle;
use uuid::Uuid;

#[derive(Debug)]
pub struct Applications {
    to_uuid: HashMap<String, Uuid>,
    installed: HashMap<Uuid, ParticleManifest>,
    running: HashMap<Uuid, Application<Message>>,
}

impl Applications {
    #![allow(dead_code)]
    pub fn new() -> Applications {
        Applications {
            to_uuid: HashMap::new(),
            installed: HashMap::new(),
            running: HashMap::new(),
        }
    }
    pub fn get_uuid<S>(&self, name: S) -> Result<Uuid>
        where S: Into<String>
    {
        let name: String = name.into();
        self.to_uuid
            .get(&name)
            .ok_or(Error::new(ErrKind::App, format!("{} not found", name)))
            .map(|id| -> Uuid { id.clone() })
    }
    pub fn get_name(&self, id: Uuid) -> Result<String> {
        self.installed
            .get(&id)
            .ok_or(Error::new(ErrKind::App, format!("{} not found", id)))
            .map(|app| -> String { app.name.clone() })
    }
    pub fn get_instance(&self, id: Uuid) -> Result<&Application<Message>> {
        self.running
            .get(&id)
            .ok_or(Error::new(ErrKind::App, format!("{} is not running", id)))
    }
    pub fn get_running(&self) -> HashSet<Uuid> {
        self.running.keys().map(|id| id.clone()).collect()
    }
    pub fn is_stopped(&self, id: Uuid) -> Result<()> {
        match self.get_instance(id) {
            Ok(_) => Err(Error::new(ErrKind::App, format!("{} is already running", id))),
            Err(_) => Ok(()),
        }
    }
    pub fn start(&mut self, id: Uuid, app: Application<Message>) -> Result<()> {
        self.is_stopped(id)?;
        self.running.insert(id, app);
        Ok(())
    }
    pub fn stop(&mut self, id: Uuid) -> Result<()> {
        let app: Application<Message> =
            self.running
                .remove(&id)
                .ok_or(Error::new(ErrKind::App, format!("{} is not running", id)))?;
        app.stop()
    }
    pub fn get_manifest(&self, id: Uuid) -> Result<&ParticleManifest> {
        self.installed
            .get(&id)
            .ok_or(Error::new(ErrKind::App, format!("{} is not installed", id)))
    }
    pub fn get_installed(&self) -> HashSet<ParticleManifest> {
        self.installed.values().map(|p| p.clone()).collect()
    }
    pub fn is_uninstalled(&self, app: &ParticleManifest) -> Result<()> {
        match self.get_manifest(app.id) {
            Ok(_) => Err(Error::new(ErrKind::App, format!("{} is already installed", app.id))),
            Err(_) => Ok(()),
        }
    }
    pub fn install(&mut self, app: ParticleManifest) -> Result<ParticleManifest> {
        self.is_uninstalled(&app)?;
        self.to_uuid.insert(app.name.clone(), app.id);
        self.installed.insert(app.id, app.clone());
        Ok(app)
    }
    pub fn uninstall(&mut self, id: Uuid) -> Result<ParticleManifest> {
        let app: ParticleManifest =
            self.installed
                .remove(&id)
                .ok_or(Error::new(ErrKind::App, format!("{} is not installed", id)))?;
        self.to_uuid.remove(&app.name);
        Ok(app)
    }
}

#[allow(non_camel_case_types)]
#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum APP_MANAGEMENT {
    INSTALL(ParticleManifest),
    UNINSTALL(Uuid),
    START(Uuid, Uuid),
    STOP(Uuid, Uuid),
    QUERY,
}

impl APP_MANAGEMENT {
    #![allow(dead_code)]
    pub fn start(src: Uuid, sig: Signature, app: Uuid) -> Message {
        MessageBuilder::new("APP_MANAGEMENT")
            .source(Signature::atom(src))
            .destination(sig)
            .data(&APP_MANAGEMENT::START(app, src))
            .unwrap()
            .trace("APP_MANAGEMENT", src)
            .build_packet()
    }

    pub fn stop(src: Uuid, sig: Signature, app: Uuid) -> Message {
        MessageBuilder::new("APP_MANAGEMENT")
            .source(Signature::atom(src))
            .destination(sig)
            .data(&APP_MANAGEMENT::STOP(app, src))
            .unwrap()
            .trace("APP_MANAGEMENT", src)
            .build_packet()
    }

    pub fn uninstall(src: Uuid, sig: Signature, app: Uuid) -> Message {
        MessageBuilder::new("APP_MANAGEMENT")
            .source(Signature::atom(src))
            .destination(sig)
            .data(&APP_MANAGEMENT::UNINSTALL(app))
            .unwrap()
            .trace("APP_MANAGEMENT", src)
            .build_packet()
    }

    pub fn install(src: Uuid, sig: Signature, app: ParticleManifest) -> Message {
        MessageBuilder::new("APP_MANAGEMENT")
            .source(Signature::atom(src))
            .destination(sig)
            .data(&APP_MANAGEMENT::INSTALL(app))
            .unwrap()
            .trace("APP_MANAGEMENT", src)
            .build_packet()
    }

    pub fn query(src: Uuid) -> Message {
        MessageBuilder::new("APP_MANAGEMENT")
            .source(Signature::atom(src))
            .destination(Signature::broadcast())
            .data(&APP_MANAGEMENT::QUERY)
            .unwrap()
            .trace("APP_MANAGEMENT", src)
            .build_packet()
    }
}

#[allow(non_snake_case)]
pub fn apps___startup(handle: &Handle,
                      state: Arc<State>,
                      install: Vec<ParticleManifest>,
                      startup: &Vec<String>,
                      client: ReqesClient<Message>,
                      atom: Uuid)
                      -> Result<()> {
    let mut t_startup: HashSet<String> = HashSet::new();
    for app in startup {
        t_startup.insert(app.clone());
    }
    let t_client = client.clone();
    let req = APP_MANAGEMENT::query(atom);
    let mut fut: Box<Future<Item = Message, Error = Error>> = Box::new(t_client.send(req));
    for app in install.clone() {
        let client = client.clone();
        let req = APP_MANAGEMENT::install(atom, Signature::broadcast(), app);
        fut = Box::new(fut.then(move |_| client.send(req)));
    }
    for app in install {
        if t_startup.get(&app.name).is_some() {
            let client = client.clone();
            let req = APP_MANAGEMENT::start(atom, Signature::broadcast(), app.id);
            fut = Box::new(fut.then(move |_| client.send(req)));
        }
    }
    let state = state.clone();
    handle.spawn(fut.then(move |_| -> BoxFuture<(), ()> {
                                   info!("state:\r\n{:?}", state);
                                   future::ok(()).boxed()
                               }));
    Ok(())
}

#[allow(non_snake_case)]
pub fn handler___app_management(handle: Handle,
                                out_client: ReqesClient<Message>,
                                state: Arc<State>,
                                applications: Arc<Mutex<Applications>>,
                                local_id: Uuid,
                                msg: Message)
                                -> Box<Future<Item = Message, Error = Error>> {
    let src_id: Uuid = msg.get_source().device.get_id();
    let action: Result<APP_MANAGEMENT> = msg.get_data();
    let data = match action {
        Ok(APP_MANAGEMENT::INSTALL(app)) => app_management___install(state, applications, app),
        Ok(APP_MANAGEMENT::UNINSTALL(app)) => app_management___uninstall(state, applications, app),
        Ok(APP_MANAGEMENT::START(app, target_id)) => {
            app_management___start(handle,
                                   out_client,
                                   state,
                                   applications,
                                   local_id,
                                   app,
                                   target_id)
        }
        Ok(APP_MANAGEMENT::STOP(app, target_id)) => {
            app_management___stop(state, applications, local_id, app, target_id)
        }
        Ok(APP_MANAGEMENT::QUERY) => {
            app_management___query(applications, out_client, local_id, src_id)
        }
        _ => future::err(Error::new(ErrKind::Other, format!("{:?}", action))).boxed(),
    };
    let fut = data.then(|res| {
                            future::ok(msg.response()
                                           .data(res)
                                           .trace("APP_MANAGEMENT", Uuid::nil())
                                           .build_packet())
                        });
    Box::new(fut)
}

#[allow(non_snake_case)]
fn app_management___install(state: Arc<State>,
                            applications: Arc<Mutex<Applications>>,
                            app: ParticleManifest)
                            -> BStateFuture {
    let mut applications = ftry!(applications.lock());
    warn!("APP_MANAGEMENT: install {:?}", app.name);
    let app = ftry!(applications.install(app));
    state.injest(update::add_application(&app))
}

#[allow(non_snake_case)]
fn app_management___uninstall(state: Arc<State>,
                              applications: Arc<Mutex<Applications>>,
                              app_id: Uuid)
                              -> BStateFuture {
    let mut applications = ftry!(applications.lock());
    info!("APP_MANAGEMENT: uninstall {:?}",
          applications.get_name(app_id));
    let app = ftry!(applications.uninstall(app_id));
    state.injest(update::remove_application(&app))
}

#[allow(non_snake_case)]
fn app_management___start(handle: Handle,
                          out_client: ReqesClient<Message>,
                          state: Arc<State>,
                          applications: Arc<Mutex<Applications>>,
                          local_id: Uuid,
                          app_id: Uuid,
                          target_id: Uuid)
                          -> BStateFuture {
    if local_id == target_id {
        let app: ParticleManifest = {
            let applications = ftry!(applications.lock());
            ftry!(applications.is_stopped(app_id));
            warn!("APP_MANAGEMENT: start {:?} on {:?}", applications.get_name(app_id), target_id);
            ftry!(applications.get_manifest(app_id)).clone()
        };
        let signature = Signature::new(RoutingType::One(local_id), RoutingType::One(app.id));
        //Start Outbound App Message Server
        let (server, app_thread) = socket_server(prefix___here_app!(app.name, app___ipc_out!()))
            .unwrap();
        let t_handle = handle.clone();
        let t_out_client = out_client.clone();
        let server = server
            .receive()
            .map(move |req| (req, t_handle.clone(), t_out_client.clone(), signature.clone()))
            .and_then(|(mut req, handle, out_client, signature)| -> BoxFuture<(), Error> {
                          let mut msg: Message = req.take_data().unwrap();
                          msg.set_source(signature);
                          req.reply(&handle, out_client.send(msg));
                          future::ok(()).boxed()
                      })
            .map_err(eif!(|_| ()))
            .for_each(|_| -> BoxFuture<(), ()> { future::ok(()).boxed() });
        handle.spawn(Box::new(server));
        let fut = state
            .injest(update::start_application(app_id, target_id))
            .and_then(move |t| {
                //Start App
                let app_path = prefix___here_app!(app.name, app___bin!());
                if fs::metadata(&app_path).is_err() {
                    error!("No Binary: {}", app_path);
                    ftry!(Err(Error::new(ErrKind::Other, format!("No Binary: {}", app_path))));
                }
                info!("Start: {}", app_path);
                let app_process = Command::new(prefix___here!(app___bin!()))
                    .current_dir(prefix___here!(tmpl___app!(app.name)))
                    .spawn()
                    .unwrap();
                //Start App Message Client
                let client = ftry!(socket_client(handle.clone(),
                                                 prefix___here_app!(app.name, app___ipc_in!())));

                let mut applications = ftry!(applications.lock());
                ftry!(applications.start(app.id,
                                         Application::new(app_process, app_thread, client)));
                future::ok(t).boxed()
            })
            .or_else(move |_| state.injest(update::stop_application(app_id, target_id)));

        Box::new(fut)
    } else {
        let applications = ftry!(applications.lock());
        warn!("APP_MANAGEMENT: start_register {:?}",
              applications.get_name(app_id));
        state.injest(update::start_application(app_id, target_id))
    }

}

#[allow(non_snake_case)]
fn app_management___stop(state: Arc<State>,
                         applications: Arc<Mutex<Applications>>,
                         local_id: Uuid,
                         app_id: Uuid,
                         target_id: Uuid)
                         -> BStateFuture {
    let mut applications = ftry!(applications.lock());
    if local_id == target_id {
        warn!("APP_MANAGEMENT: stop {:?} on {:?}", applications.get_name(app_id), target_id);
        ftry!(applications.stop(app_id));
    } else {
        warn!("APP_MANAGEMENT: stop_deregister {:?}",
              applications.get_name(app_id));
    }
    state.injest(update::stop_application(app_id, target_id))
}

#[allow(non_snake_case)]
fn app_management___query(applications: Arc<Mutex<Applications>>,
                          client: ReqesClient<Message>,
                          local_id: Uuid,
                          for_id: Uuid)
                          -> BStateFuture {
    let applications = ftry!(applications.lock());
    warn!("APP_MANAGEMENT: query {:?} from {:?}", local_id, for_id);
    if local_id != for_id {
        let mut fut: Option<Box<Future<Item = Message, Error = Error>>> = None;
        for app in applications.get_installed() {
            let client = client.clone();
            let req = APP_MANAGEMENT::install(local_id, Signature::atom(for_id), app);
            fut = match fut {
                Some(fut) => Some(Box::new(fut.then(move |_| client.send(req)))),
                None => Some(client.send(req)),
            };
        }
        for app in applications.get_running() {
            let client = client.clone();
            let req = APP_MANAGEMENT::start(local_id, Signature::atom(for_id), app);
            fut = match fut {
                Some(fut) => Some(Box::new(fut.then(move |_| client.send(req)))),
                None => Some(client.send(req)),
            };
        }
        match fut {
            Some(fut) => Box::new(fut.and_then(|_| future::ok(Transaction::True))),
            None => Box::new(future::ok(Transaction::True)),
        }
    } else {
        Box::new(future::ok(Transaction::True))
    }
}