use manifest::*;
use state::State;
use state::action::*;
use state::redis::*;
use uuid::Uuid;

/// Setup the Redis Database
pub fn new(con: &RedisClient) -> Vec<StateAction> {
    con.set("root", &State::root_record()).unwrap();
    vec![StateAction::create("self", &()).unwrap(),
         StateAction::create("data", &()).unwrap()]
}

/// Reset group and self
pub fn reset() -> Vec<StateAction> {
    vec![StateAction::delete("group"),
         StateAction::delete("self.atom"),
         StateAction::delete("self.device"),
         StateAction::delete("self.startup")]
}


/// Init group and self info
pub fn init(manifest: &Manifest) -> StateAction {
    vec![vec![StateAction::create("group", &()).unwrap(),
              StateAction::create("group.devices", &()).unwrap(),
              StateAction::create("group.particles", &()).unwrap(),
              StateAction::create("group.actions", &()).unwrap(),
              StateAction::create("self.atom", &manifest.atom).unwrap(),
              StateAction::create("self.device", &manifest.device).unwrap(),
              StateAction::create("self.startup", &manifest.particles).unwrap(),
              add_action("STATE_ACTION"),
              add_action("APP_MANAGEMENT")]
                 .into(),
         add_permission("STATE_ACTION", "send", manifest.atom.network_id)]
            .into()
}

pub fn add_action(action: &str) -> StateAction {
    let actions =
        vec![StateAction::create(format!("group.actions.{}", action), &()).unwrap(),
             StateAction::create(format!("group.actions.{}.send", action), &()).unwrap(),
             StateAction::create(format!("group.actions.{}.receive", action), &()).unwrap(),
             StateAction::create(format!("group.actions.{}.broadcast", action), &()).unwrap()];
    StateAction::conditional(format!("group.actions.{}", action),
                             Operator::Not(Box::new(Operator::Exists)),
                             Some(actions.into()))
}

pub fn add_permission(action: &str, permission: &str, particle: Uuid) -> StateAction {
    let action: String = action.into();
    vec![add_action(&action),
         StateAction::create(format!("group.actions.{}.{}.{}", action, permission, particle),
                             &())
                 .unwrap()]
            .into()
}

pub fn remove_permission(action: &str, permission: &str, app: Uuid) -> StateAction {
    StateAction::delete(format!("group.actions.{}.{}.{}", action, permission, app))
}

pub fn add_device(device: &DeviceManifest) -> StateAction {
    vec![StateAction::create(format!("group.devices.{}", device.id), device).unwrap(),
         add_application(&device_app(device.id)),
         start_application(device.id, device.id)]
            .into()
}

fn device_app(device: Uuid) -> ParticleManifest {
    ParticleManifest {
        id: device,
        name: "".into(),
        permissions: PermissionsManifest {
            send: vec!["APP_MANAGEMENT".into()],
            receive: vec!["STATE_ACTION".into(),
                          "APP_MANAGEMENT".into()],
            broadcast: vec!["APP_MANAGEMENT".into()],
        },
    }
}

pub fn add_application(particle: &ParticleManifest) -> StateAction {
    let mut list: Vec<StateAction> = Vec::new();
    list.push(StateAction::create(format!("group.particles.{}", particle.id), particle).unwrap());
    for send in &particle.permissions.send {
        list.push(add_permission(&send, "send", particle.id));
    }
    for receive in &particle.permissions.receive {
        list.push(add_permission(&receive, "receive", particle.id));
    }
    for broadcast in &particle.permissions.broadcast {
        list.push(add_permission(&broadcast, "broadcast", particle.id));
    }
    list.into()
}

pub fn remove_application(app: &ParticleManifest) -> StateAction {
    let mut list: Vec<StateAction> = Vec::new();
    list.push(StateAction::delete(format!("group.particles.{}", app.id)));
    for send in &app.permissions.send {
        list.push(remove_permission(&send, "send", app.id));
    }
    for receive in &app.permissions.receive {
        list.push(remove_permission(&receive, "receive", app.id));
    }
    for broadcast in &app.permissions.broadcast {
        list.push(remove_permission(&broadcast, "broadcast", app.id));
    }
    list.into()
}

pub fn start_application(app: Uuid, device: Uuid) -> StateAction {
    vec![StateAction::create(format!("group.particles.{}.{}", app, device), &()).unwrap(),
         StateAction::create(format!("group.devices.{}.{}", device, app), &()).unwrap()]
            .into()
}

pub fn stop_application(app: Uuid, device: Uuid) -> StateAction {
    vec![StateAction::delete(format!("group.particles.{}.{}", app, device)),
         StateAction::delete(format!("group.devices.{}.{}", device, app))]
            .into()
}