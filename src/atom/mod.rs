use err::*;
use futures::{BoxFuture, Future, Stream, future};
use handler::AtomClient;
use log::LogLevel;
use manifest::*;
use message::{Message, RoutingType, Signature};
use networking::{Client, NetworkClient, NetworkServer};
use networking::reqes::*;
use path::*;
use permissions::Permissions;
use state::State;
use state::action::*;
use state::record::*;
use state::redis::*;
use std::collections::HashSet;
use std::fmt;
use std::fs;
use std::sync::{Arc, Mutex};
use tokio_core::reactor::{Core, Handle};
use uuid::Uuid;

mod update;
mod app_manager;
use self::app_manager::*;

use std::str::FromStr;

pub struct Atom {
    reqes_in: Reqes<Message>,
    reqes_out: Reqes<Message>,
    state: Arc<State>,
    manifest: Manifest,
    apps_list: Vec<ParticleManifest>
}
impl Atom {
    pub fn new() -> Atom {
        //Request Response Pairs
        let reqes_in = reqes::<Message>();
        let reqes_out = reqes::<Message>();

        //Load Manifest Files
        let redis: RedisManifest = open_file(&prefix___here!(atm___redis!())).unwrap();
        let manifest: Manifest = open_file(&prefix___here!(atm___manifest!())).unwrap();

        let apps_list = open_dir::<ParticleManifest>(&prefix___here!(app___fldr!()),
                                                     &prefix___fldr!(app___manifest!()))
                .unwrap();
        let device_list: Vec<DeviceManifest> = open_dir(&prefix___here!(atm___fldr!()), "").unwrap();

        if fs::metadata(&redis.socket).is_err() {
            panic!("socket {} doesn't exist", redis.socket);
        }

        //Start State Manager
        let db = RedisClientPool::new(&redis).unwrap();
        let con: RedisClient = db.get_con().unwrap();
        let state: Arc<State> = Arc::new(State::new(db));

        //Create Root Directories
        con.flush().unwrap();
        if !con.exists("root").unwrap() {
            state.injest_sync(update::new(&con)).unwrap();
        } else {
            state.injest_sync(update::reset()).unwrap();
        }

        state.injest_sync(update::init(&manifest)).unwrap();

        //Add Devices to allow communication
        for device in &device_list {
            state.injest_sync(update::add_device(device)).unwrap();
        }

        debug!("{:?}", state);

        // Return Atom
        Atom {
            reqes_in: reqes_in,
            reqes_out: reqes_out,
            state: state,
            manifest: manifest,
            apps_list: apps_list
        }
    }

    /// Start Processing Incoming Messages
    pub fn run(self, mut core: Core) {

        let atom = self.manifest.atom;
        let local_device = self.manifest.device;


        //Permissions Manager
        let permissions = Arc::new(Permissions::new());

        // Create AtomClient
        let device = RoutingType::One(local_device.id);
        let net_sig = Signature::new(device.clone(), RoutingType::One(atom.network_id));
        let atom_sig = Signature::new(device, RoutingType::One(local_device.id));

        let ac = AtomClient::new(atom.network_id,
                                 self.reqes_in.client.clone(),
                                 net_sig,
                                 atom_sig);

        // Start Network Client
        let net_client = NetworkClient::new(ac.clone(), &core.handle());
        let net_client = Arc::new(net_client);

        // Start Network Server
        NetworkServer::new(ac.clone(), local_device.address).forwarder();


        // Generate App Startup Messages
        let applications = Applications::new();
        apps___startup(&core.handle(),
                       self.state.clone(),
                       self.apps_list,
                       &self.manifest.particles,
                       self.reqes_out.client.clone(),
                       local_device.id)
                .unwrap();

        let applications = Arc::new(Mutex::new(applications));

        let in_stream = Atom::inbound_message(core.handle(),
                                              self.reqes_in.server,
                                              self.reqes_out.client,
                                              permissions.clone(),
                                              self.state.clone(),
                                              applications.clone(),
                                              local_device.id);

        let out_stream = Atom::outbound_message(core.handle(),
                                                self.reqes_out.server,
                                                net_client,
                                                permissions,
                                                self.state,
                                                local_device.id);

        core.handle().spawn(out_stream);
        let run = core.run(in_stream);
        result!(LogLevel::Info,
                run,
                "run listener in core",
                "unexpected event loop termination");
    }

    /// Send a Debug type message through the system and output the result
    pub fn _debug<F>(&self, handle: &Handle, m: Message, f: F)
        where F: Fn(Message) + 'static
    {
        handle.spawn(self.reqes_out
                         .client
                         .send(m)
                         .then(move |res| {
            result!(LogLevel::Info, res, "received result", "received error");
            info!("{:?}", res);
            match res {
                Ok(msg) => {
                    f(msg);
                    future::ok(())
                }
                _ => future::err(()),
            }
        }));
    }

    fn inbound_message(handle: Handle,
                       in_server: ReqesServer<Message>,
                       out_client: ReqesClient<Message>,
                       permissions: Arc<Permissions>,
                       state: Arc<State>,
                       applications: Arc<Mutex<Applications>>,
                       device_id: Uuid)
                       -> Box<Future<Item = (), Error = ()>> {
        let in_server = in_server
            .receive()
            .map(move |req: Request<Message>| {
                (req,
                 handle.clone(),
                 out_client.clone(),
                 permissions.clone(),
                 state.clone(),
                 applications.clone(),
                 device_id)
            })
            .and_then(|(mut req,
                        handle,
                        out_client,
                        permissions,
                        state,
                        applications,
                        device_id)|
                       -> BoxFuture<(), Error> {
                let msg: Message = req.take_data().unwrap();
                trace!("Incoming Message\r\n{:?}", msg);

                log!(if msg.get_action() == "STATE_ACTION" {
                         LogLevel::Trace
                     } else {
                         LogLevel::Info
                     },
                     "{:?} requesting {:?} on {:?}",
                     msg.get_source().particle,
                     msg.get_action(),
                     msg.get_destination().particle);

                let err_state = state.clone();
                let t_handle = handle.clone();
                let request_future = permissions
                    .injest(state.clone(), msg, device_id.clone())
                    .map(move |m: Message| {
                             (m, t_handle, device_id, out_client, state, applications)
                         })
                    .then(|res| -> Box<Future<Item = Message, Error = Error>> {
                        if res.is_err() {
                            future::ok(res.unwrap_err()).boxed()
                        } else {
                            let (mut msg, handle, device_id, out_client, state, applications) =
                                res.unwrap();
                            msg.get_trace().add("Atom::incoming", device_id);

                            let action: String = msg.get_action().clone().into();
                            // Route to Permissions
                            match action.as_ref() {
                                "STATE_ACTION" => Atom::state_handler(device_id, state, msg),
                                "APP_MANAGEMENT" => {
                                    handler___app_management(handle,
                                                             out_client,
                                                             state,
                                                             applications,
                                                             device_id,
                                                             msg)
                                }
                                "ADVERTISE" => Atom::app_handler(msg),
                                action => {
                                    Atom::router(action.into(),
                                                 device_id,
                                                 state,
                                                 applications,
                                                 msg)
                                }
                            }
                        }
                    })
                    .map_err(eif!(move |e| {
                                      error!("{:?}", err_state);
                                      e
                                  }));
                req.reply(&handle, request_future);
                future::ok(()).boxed()
            })
            .map_err(eif!(|_| ()))
            .for_each(|_| -> BoxFuture<(), ()> { future::ok(()).boxed() });
        Box::new(in_server)
    }

    fn outbound_message(handle: Handle,
                        out_server: ReqesServer<Message>,
                        network: Arc<NetworkClient>,
                        permissions: Arc<Permissions>,
                        state: Arc<State>,
                        device_id: Uuid)
                        -> Box<Future<Item = (), Error = ()>> {
        let out_server =
            out_server
                .receive()
                .map(move |req: Request<Message>| {
                    (req,
                    device_id,
                    network.clone(),
                    permissions.clone(),
                    state.clone(),
                    handle.clone())
                })
                .and_then(|(mut req,
                            device_id,
                            network,
                            permissions,
                            state,
                            handle)|
                            -> BoxFuture<(), Error> {
                    let msg: Message = req.take_data().unwrap();
                    trace!("Outgoing Message\r\n{:?}", msg);

                    log!(if msg.get_action() == "STATE_ACTION" {
                             LogLevel::Trace
                         } else {
                             LogLevel::Info
                         },
                         "{:?} requesting {:?} on {:?}",
                         msg.get_source().particle,
                         msg.get_action(),
                         msg.get_destination().particle);

                    let err_state = state.clone();
                    let request_future = permissions
                        .injest(state.clone(), msg, device_id.clone())
                        .map(move |m: Message| (m, device_id, network))
                        .then(|res| -> Box<Future<Item = Message, Error = Error>> {
                            if res.is_err() {
                                future::ok(res.unwrap_err()).boxed()
                            } else {
                                let (mut msg, device_id, network) = res.unwrap();
                                msg.get_trace().add("Atom::run()", device_id);

                            // Route to Network
                            let network = network
                                .request(msg)
                                .map_err(eif!(Error::from));
                            Box::new(network)
                            }
                        })
                        .map_err(eif!(move |e| {
                                        error!("{:?}", err_state);
                                        e
                                    }));
                    req.reply(&handle, request_future);
                    future::ok(()).boxed()
                })
                .map_err(eif!(|_| ()))
                .for_each(|_| -> BoxFuture<(), ()> { future::ok(()).boxed() });
        Box::new(out_server)
    }

    fn state_handler(device_id: Uuid,
                     state: Arc<State>,
                     msg: Message)
                     -> Box<Future<Item = Message, Error = Error>> {

        let state_action = msg.get_data::<StateAction>().unwrap();
        state
            .injest(state_action)
            .then(move |t| {
                      future::ok(msg.response()
                                     .data(t)
                                     .trace("Atom::state", device_id)
                                     .build_packet())
                  })
            .boxed()
    }

    fn app_handler(_: Message) -> Box<Future<Item = Message, Error = Error>> {
        unimplemented!()
    }

    fn router(action: String,
              device_id: Uuid,
              state: Arc<State>,
              applications: Arc<Mutex<Applications>>,
              msg: Message)
              -> Box<Future<Item = Message, Error = Error>> {
        //Get applications running on this device
        let action: String = action.into();
        let router = state
            .injest(StateAction::read(format!("group.devices.{}", device_id)))
            .map(|result: Transaction| -> Record { result.first().unwrap() })
            .then(move |res: Result<Record>| -> Box<Future<Item = (Vec<String>, Message), Error = Error>> {
                if res.is_err() {
                    return future::err(res.unwrap_err()).boxed();
                }
                let record = res.unwrap();
                //Determine the candidate destinations
                let mut routing_candidates: Vec<String> = Vec::new();
                let routing_candidates = match msg.get_destination().particle.clone() {
                    RoutingType::One(ref app) => {
                        routing_candidates.push(format!("{}", app));
                        future::ok(routing_candidates).boxed()
                    }
                    RoutingType::Some(ref apps) => {
                        for app in apps {
                            routing_candidates.push(format!("{}", app));
                        }
                        future::ok(routing_candidates).boxed()
                    }
                    RoutingType::Any | RoutingType::All => {
                        let state_action = StateAction::read(format!("group.actions.{}.receive", action));
                        Box::new(state
                                     .injest(state_action)
                                     .map(|result: Transaction| result.first().unwrap().children.into_iter().collect()))
                    }
                };

                //Ensure destination candidates are running on this device
                let device_applications: HashSet<String> = record
                    .children
                    .into_iter()
                    .map(|key| Record::get_name(key))
                    .collect();
                let routing_list = routing_candidates
                    .map(|apps| (apps, msg))
                    .and_then(move |(apps, msg): (Vec<String>, Message)| {
                        let mut routing_list: Vec<String> = Vec::new();
                        for app in apps {
                            match device_applications.get(&app) {
                                Some(app) => routing_list.push(app.clone()),
                                None => {
                                    match msg.get_destination().particle.clone() {
                                        RoutingType::One(_) |
                                        RoutingType::Some(_) => {
                                            warn!("app_id {} not found in device. particles: {:?}",
                                                  app,
                                                  device_applications);
                                            return future::err(Error::new(ErrKind::Invariant, "Particle in Signature MUST be present on the atom")).boxed();
                                        }
                                        _ => (),
                                    }
                                }
                            }
                        }
                        if routing_list.is_empty() {
                            future::err(Error::new(ErrKind::Invariant, "Particle in Signature MUST be able to be resolved")).boxed()
                        } else {
                            future::ok((routing_list, msg)).boxed()
                        }
                    });
                routing_list.boxed()
            })
            .then(move |res: Result<(Vec<String>, Message)>| match res {
                      Ok((routing_list, msg)) => {
                          let res = applications.lock();
                          match res {
                              Err(e) => future::err(Error::from(e)).boxed(),
                              Ok(apps) => {
                                  match apps.get_instance(Uuid::from_str(&routing_list[0]).unwrap()) {
                                      Ok(app) => app.client().send(msg),
                                      Err(e) => future::err(e).boxed()
                                  }
                              }
                          }
                      },
                      Err(e) => future::err(e).boxed(),
                  });
        Box::new(router)
    }
}

impl fmt::Debug for Atom {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Atom {{ in: {:?}, out: {:?}, state: {:?} }}",
               self.reqes_in,
               self.reqes_out,
               self.state)
    }
}
