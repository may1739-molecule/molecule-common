use err::*;
use futures::{BoxFuture, Future, Stream, future};
use message::Message;
use networking::{Client, Server, SocketClient, SocketServer};
use networking::reqes::*;
use std::fs;
use std::io;
use std::process::Child;
use std::sync::Arc;
use std::thread::{self, JoinHandle};
use std::time::Duration;
use tokio_core::reactor::Handle;
use tokio_service::Service;

pub fn start(handle: Handle) -> Result<Reqes<Message>> {

    let (in_server, _) = socket_server(prefix___here!(app___ipc_in!()).into())?;

    let out_client = socket_client(handle, prefix___here!(app___ipc_out!()).into())?;

    Ok(Reqes {
           client: out_client,
           server: in_server,
       })
}

pub fn socket_server(path: String) -> Result<(ReqesServer<Message>, JoinHandle<()>)> {
    let reqes = reqes::<Message>();
    let client = reqes.client.clone();
    let ipc = SocketServer::new(path.clone().into());
    let thread = ipc.serve(move || Ok(IPCService::new(client.clone())))
        .map_err(Error::from)?;
    Ok((reqes.server, thread))
}

pub fn socket_client(handle: Handle, path: String) -> Result<ReqesClient<Message>> {
    let reqes = reqes::<Message>();

    //Wait for Socket to exist
    for _ in 0..20 {
        if fs::metadata(&path).is_err() {
            thread::sleep(Duration::from_millis(100));
        } else {
            break;
        }
    }
    //Timeout in 2 seconds
    if fs::metadata(&path).is_err() {
        Err(Error::new(ErrKind::Other, format!("Socket {} not found", path)))?;
    }

    let ipc = Arc::new(SocketClient::new(&handle, path.into()));
    let t_handle = handle.clone();

    let server = reqes
        .server
        .receive()
        .map(move |req| (req, t_handle.clone(), ipc.clone()))
        .and_then(|(mut req, t_handle, ipc)| -> BoxFuture<(), Error> {
                      let msg = req.take_data().unwrap();

                      req.reply(&t_handle, ipc.request(msg));
                      future::ok(()).boxed()
                  })
        .map_err(eif!(|_| ()))
        .for_each(|_| -> BoxFuture<(), ()> { future::ok(()).boxed() });

    handle.spawn(Box::new(server));

    Ok(reqes.client)
}

#[derive(Debug)]
pub struct Application<M> {
    process: Child,
    thread: JoinHandle<()>,
    client: ReqesClient<M>,
}

impl<M> Application<M>
    where M: 'static
{
    pub fn new(process: Child, thread: JoinHandle<()>, client: ReqesClient<M>) -> Application<M> {
        Application {
            process: process,
            thread: thread,
            client: client,
        }
    }
    pub fn stop(mut self) -> Result<()> {
        self.process.kill()?;
        //TODO stop IPC Server but all we have is a thread handle
        Ok(())
    }
    pub fn client(&self) -> ReqesClient<M> {
        self.client.clone()
    }
}

struct IPCService {
    tx: ReqesClient<Message>,
}
impl IPCService {
    pub fn new(tx: ReqesClient<Message>) -> IPCService {
        IPCService { tx: tx }
    }
}
impl Service for IPCService {
    type Request = Message;
    type Response = Message;
    type Error = io::Error;
    type Future = Box<Future<Item = Message, Error = io::Error>>;

    fn call(&self, req: Self::Request) -> Self::Future {
        //println!("Received: {:?}", req);
        let send = self.tx
            .send(req)
            .map_err(eif!(|e| io::Error::new(io::ErrorKind::Other, e)));
        Box::new(send)
    }
}
