

use err::{ErrKind, Error};

use futures::{BoxFuture, Future, future};
use message::{Message, RoutingType};
use state::State;
use state::action::*;
use std::fmt;
use std::sync::Arc;

use uuid::Uuid;

pub struct Permissions {}

impl Permissions {
    pub fn new() -> Permissions {
        Permissions {}
    }
    pub fn injest(&self,
                  state: Arc<State>,
                  msg: Message,
                  atom_id: Uuid)
                  -> BoxFuture<Message, Message> {
        Permissions::ttl(msg, atom_id)
            .and_then(move |msg| match msg.get_destination().particle.clone() {
                          RoutingType::Any |
                          RoutingType::One(_) |
                          RoutingType::Some(_) => {
                              Permissions::send(state.clone(), msg, atom_id)
                                  .and_then(move |msg| {
                                                Permissions::receive(state, msg)
                                            })
                                  .boxed()
                          }
                          RoutingType::All => {
                              Permissions::send_broadcast(state.clone(), msg, atom_id)
                          }
                      })
            .boxed()
    }
    fn ttl(mut msg: Message, atom_id: Uuid) -> BoxFuture<Message, Message> {
        if msg.get_trace().ttl() < 50 {
            future::ok(msg).boxed()
        } else {
            future::err(msg.response()
                            .data::<()>(Err(Error::new(ErrKind::Permissions,
                                                       "message has been floating around too long")))
                            .trace("Permissions", atom_id)
                            .build_packet())
                    .boxed()
        }
    }
    fn send(state: Arc<State>, msg: Message, atom_id: Uuid) -> BoxFuture<Message, Message> {
        let id: Uuid = msg.get_source().particle.get_id();
        let action: String = msg.get_action().into();
        state
            .injest(StateAction::read(format!("group.actions.{}.send.{}", action, id)))
            .then(move |res| if res.is_ok() {
                      future::ok(msg)
                  } else {
                      future::err(msg.response()
                                      .data::<()>(Err(Error::new(ErrKind::Permissions,
                                                                 format!("Permissions Denied: {} cannot send \
                                                        '{}'",
                                                                         id,
                                                                         action))))
                                      .trace("Permissions", atom_id)
                                      .build_packet())
                  })
            .boxed()
    }
    fn error(e_msg: String, msg: Message) -> Message {
        msg.response()
            .data::<()>(Err(Error::new(ErrKind::Permissions,
                            format!("Permissions Denied: {}", e_msg))))
                                      .trace("Permissions", Uuid::nil())
                                      .build_packet()
    }

    fn receive(state: Arc<State>, msg: Message) -> BoxFuture<Message, Message> {
        
        let action: String = msg.get_action().into();
        match msg.get_destination().particle.clone() {
            RoutingType::All => unreachable!(),
            RoutingType::Some(ids) => {
                let future_vec = ids.into_iter().map(move |id| Self::receive_helper(
                        state.clone(),
                        format!("group.actions.{}.receive.{}", action, id),
                        Operator::Exists,
                        format!("{} cannot receive '{}'", id, action)
                ));
                Self::to_permissions_future(future::join_all(future_vec).map(|_| ()).boxed(), msg)
            },
            RoutingType::One(id) => Self::to_permissions_future(Self::receive_helper(
                state,
                format!("group.actions.{}.receive.{}", action, id),
                Operator::Exists,
                format!("{} cannot receive '{}'", id, action)
            ), msg),
            RoutingType::Any => {
                match  msg.get_destination().device.clone() {
                    RoutingType::All | RoutingType::Any => Self::to_permissions_future(Self::receive_helper(
                        state,
                        format!("group.actions.{}.receive", action),
                        Operator::Exists,
                        format!("system cannot receive '{}'", action)
                    ), msg),
                    RoutingType::Some(ids) => {
                        let future_vec = ids.into_iter().map(move |id| Self::receive_helper(
                            state.clone(),
                            format!("group.actions.{}.receive", action),
                            Operator::Intersection(format!("group.devices.{}", id)),
                            format!("device {} cannot receive '{}'", id, action)
                        ));
                        Self::to_permissions_future(future::join_all(future_vec).map(|_| ()).boxed(), msg)
                    }
                    RoutingType::One(id) => Self::to_permissions_future(Self::receive_helper(
                        state,
                        format!("group.actions.{}.receive", action),
                        Operator::Intersection(format!("group.devices.{}", id)),
                        format!("device {} cannot receive '{}'", id, action)
                    ), msg),
                }
            }
        }
    }
    fn receive_helper(state: Arc<State>, key: String, op: Operator, e_msg: String) -> BoxFuture<(), String> {
        state.injest(StateAction::conditional(
            key,
            op,
            None)
        ).then(|res| {
            match res {
                Ok(r) => match r.is_truthy() {
                    true => future::ok(()).boxed(),
                    false => future::err(e_msg).boxed()
                },
                Err(_) => future::err(e_msg).boxed()
            }
        }).boxed()
    }
    fn to_permissions_future(f: BoxFuture<(), String>, msg: Message) -> BoxFuture<Message, Message> {
        f.then(|res| {
            match res {
                Ok(_) => future::ok(msg).boxed(),
                Err(e) => future::err(Permissions::error(e, msg)).boxed()
            }
        }).boxed()
    }

    fn send_broadcast(state: Arc<State>,
                      msg: Message,
                      atom_id: Uuid)
                      -> BoxFuture<Message, Message> {
        let id: Uuid = msg.get_source().particle.get_id();
        let action: String = msg.get_action().into();
        state
            .injest(StateAction::read(format!("group.actions.{}.broadcast.{}", action, id)))
            .then(move |res| if res.is_ok() {
                      future::ok(msg)
                  } else {
                      future::err(msg.response()
                                      .data::<()>(Err(Error::new(ErrKind::Permissions,
                                                                 format!("Permissions Denied: {} cannot broadcast '{}'",
                                                                         id,
                                                                         action))))
                                      .trace("Permissions", atom_id)
                                      .build_packet())
                  })
            .boxed()
    }
}

impl fmt::Debug for Permissions {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Permissions")
    }
}
