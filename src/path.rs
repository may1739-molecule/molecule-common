//App Path Constants
#[macro_export]
macro_rules! tmpl___app                         { ($n:expr) => (format!("app/{}", $n))}
#[macro_export]
macro_rules! app___fldr                         { () => ("app")}
#[macro_export]
macro_rules! app___manifest                     { () => (".app")}
#[macro_export]
macro_rules! app___bin                          { () => ("app.bin")}
#[macro_export]
macro_rules! app___rs                           { () => ("app.rs")}
#[macro_export]
macro_rules! app___ipc_in                       { () => ("ipc.in")}
#[macro_export]
macro_rules! app___ipc_out                      { () => ("ipc.out")}

//Dev App Path Constants
#[macro_export]
macro_rules! tmpl___dev_app                     { ($n:expr) => (format!("molecule_app___{}", $n)) }
#[macro_export]
macro_rules! dev_app___release                  { () => ("release")}
#[macro_export]
macro_rules! dev_app___manifest                 { () => (concat!(dev_app___release!(),  "/", app___manifest!()))}
#[macro_export]
macro_rules! dev_app___data                     { () => (concat!(dev_app___release!(),  "/", "data"))}
#[macro_export]
macro_rules! dev_app___src                      { () => ("src")}
#[macro_export]
macro_rules! dev_app___rs                       { () => (concat!(dev_app___src!(),      "/", app___rs!()))}
#[macro_export]
macro_rules! dev_app___toml                     { () => ("Cargo.toml")}
#[macro_export]
macro_rules! dev_app___setup                    { () => ("setup.sh")}
#[macro_export]
macro_rules! dev_app___compile                  { () => ("release.sh")}
#[macro_export]
macro_rules! dev_app___ignore                   { () => (".gitignore")}

//Atom Path Constants
#[macro_export]
macro_rules! tmpl___atom                        { ($n:expr) => (format!("atm/{}", $n))}
#[macro_export]
macro_rules! atm___fldr                         { () => ("atm")}
#[macro_export]
macro_rules! atm___manifest                     { () => (".atom")}
#[macro_export]
macro_rules! atm___device                       { () => (".device")}
#[macro_export]
macro_rules! atm___redis                        { () => (".redis")}
#[macro_export]
macro_rules! atm___log                          { () => ("log.yaml")}

//System Path Constants
#[macro_export]
macro_rules! tmpl___sys                         { ($n:expr) => (format!("{}.sys", $n))}

#[macro_export]
macro_rules! fldr___dat                        { () => ("../dat") }
#[macro_export]
macro_rules! fldr___sys                        { () => ("../dat/sys") }
#[macro_export]
macro_rules! fldr___dev                        { () => ("../dat/dev") }
#[macro_export]
macro_rules! fldr___boot                       { () => ("../dat/boot") }


#[macro_export]
macro_rules! prefix___boot                     { ($p:expr) => (format!("../dat/boot/{}", $p)) }
#[macro_export]
macro_rules! prefix___boot_atm                 { ($n:expr, $p:expr) => (format!("../dat/boot/{}/{}", $n, $p)) }
#[macro_export]
macro_rules! prefix___sys                      { ($p:expr) => (format!("../dat/sys/{}", $p)) }
#[macro_export]
macro_rules! prefix___sys_atm                  { ($n:expr, $p:expr) => (format!("../dat/sys/{}/{}", tmpl___atom!($n), $p)) }
#[macro_export]
macro_rules! prefix___sys_app                  { ($n:expr, $p:expr) => (format!("../dat/sys/{}/{}", tmpl___app!($n), $p)) }
#[macro_export]
macro_rules! prefix___dev                      { ($p:expr) => (format!("../dat/dev/{}", $p)) }
#[macro_export]
macro_rules! prefix___dev_app                  { ($n:expr, $p:expr) => (format!("../dat/dev/{}/{}", tmpl___dev_app!($n), $p)) }
#[macro_export]
macro_rules! prefix___resources                { ($p:expr) => (format!("./resources/{}", $p)) }
#[macro_export]
macro_rules! prefix___here                     { ($p:expr) => (format!("./{}", $p)) }
#[macro_export]
macro_rules! prefix___here_atm                 { ($n:expr, $p:expr) => (format!("./{}/{}", tmpl___atom!($n), $p)) }
#[macro_export]
macro_rules! prefix___here_app                 { ($n:expr, $p:expr) => (format!("./{}/{}", tmpl___app!($n), $p)) }
#[macro_export]
macro_rules! prefix___fldr                     { ($p:expr) => (format!("/{}", $p)) }

use err::*;
use serde::{Deserialize, Serialize};
use serde_json;
use std::fs;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};

pub fn open_file<D>(path: &str) -> Result<D>
    where D: Serialize + Deserialize
{
    let mut file: fs::File = fs::File::open(path)?;
    let mut data: String = String::new();
    file.read_to_string(&mut data)?;

    Ok(serde_json::from_str::<D>(&data)?)
}

pub fn open_dir<D>(path: &str, post: &str) -> Result<Vec<D>>
    where D: Serialize + Deserialize
{
    let mut list: Vec<D> = Vec::new();
    let path: PathBuf = path.into();
    let post: String = post.into();
    if path.is_dir() {
        for entry in fs::read_dir(path)? {
            let entry = entry?.path();
            match entry.file_name() {
                Some(name) => {
                    name.to_str().ok_or("to_str")?;
                    let p = format!("{}{}", entry.to_str().ok_or("to_str")?, post);
                    list.push(open_file(&p)?)
                }
                None => (),
            }
        }
    }
    Ok(list)
}

pub fn create_file(path: &str, data: String) -> Result<()> {
    let mut file: fs::File = fs::File::create(path)?;
    file.write_all(data.as_bytes())?;
    Ok(())
}

pub fn create_dir(path: &str) -> Result<()> {
    fs::create_dir(path)?;
    Ok(())
}

pub fn copy_file(from: &str, to: &str) -> Result<()> {
    fs::copy(from, to)?;
    Ok(())
}
pub fn copy_fldr(from: &str, to: &str) -> Result<()> {
    let dir: PathBuf = from.into();
    copy_fldr_helper(&dir, to, false)
}
pub fn copy_fldr_helper(from: &Path, to: &str, copy: bool) -> Result<()> {
    println!("copy from: {}\r\n       to: {}", from.display(), to);
    if from.is_dir() {
        if copy {
            create_dir(to)?;
        }
        for entry in fs::read_dir(from)? {
            let path = entry?.path();
            match path.file_name() {
                Some(name) => {
                    let name: &str = name.to_str().ok_or("to_str")?;
                    copy_fldr_helper(&path, &format!("{}/{}", to, name), true)?;
                }
                None => (),
            }
        }
    } else if copy {
        copy_file(from.to_str().ok_or("to_str")?, to)?;
    }
    Ok(())
}