use super::{AnonProto, MessageCodec};
use super::{MessageProto, NodeProto};
use crypto;
use err;
use futures::Future;
use futures::future;
use manifest::DeviceManifest;
use message::SimpleMessage;
use state::action::StateAction;
use std::io;
use tokio_core;
#[allow(deprecated)]
use tokio_core::io::{EasyBuf, Framed, Io};
use tokio_proto::streaming::multiplex::ServerProto;
use uuid::Uuid;

#[allow(deprecated)]
impl<T: Io + 'static> ServerProto<T> for NodeProto {
    type Request = SimpleMessage;
    type RequestBody = Vec<u8>;
    type Response = SimpleMessage;
    type ResponseBody = Vec<u8>;
    type Error = io::Error;

    type Transport = Framed<T, MessageCodec>;
    type BindTransport = Box<Future<Item = Self::Transport, Error = io::Error>>;

    fn bind_transport(&self, io: T) -> Self::BindTransport {
        let atom_client = self.atom_client.clone();
        let atom_client_2 = self.atom_client.clone();
        let atom_client_3 = self.atom_client.clone();
        let handshake_future = tokio_core::io::read_exact(io, [0u8; 16])
            .map_err(err::Error::from)
            .and_then(|(socket, buf)| {
                          Uuid::from_bytes(&buf)
                              .map(|id| (socket, id))
                              .map_err(err::Error::from)
                      })
            .and_then(move |(socket, id)| {
                atom_client
                    .state(StateAction::read("self.device"))
                    .and_then(|trans| trans.first().unwrap().get_data::<DeviceManifest>())
                    .and_then(move |local_device| {
                                  let id = Vec::from(local_device.id.as_bytes().as_ref());
                                  tokio_core::io::write_all(socket, id).map_err(err::Error::from)
                              })
                    .map(move |(socket, _)| (socket, id))
            })
            .and_then(move |(socket, id)| {
                          atom_client_2
                              .state(StateAction::read(format!("group.devices.{}", id)))
                              .map(|trans| trans.first().unwrap().get_data::<DeviceManifest>())
                              .map(move |device| (socket, device))
                      })
            .map_err(io::Error::from)
            .and_then(move |(socket, device)| if let Ok(_) = device {
                          let proto = MessageProto { atom_client: atom_client_3 };
                          proto.bind_transport(socket)
                      } else {
                          let proto = AnonProto;
                          proto.bind_transport(socket)
                      });

        Box::new(handshake_future)
    }
}

#[allow(deprecated)]
impl<T: Io + 'static> ServerProto<T> for MessageProto {
    type Request = SimpleMessage;
    type RequestBody = Vec<u8>;
    type Response = SimpleMessage;
    type ResponseBody = Vec<u8>;
    type Error = io::Error;

    type Transport = Framed<T, MessageCodec>;
    type BindTransport = Box<Future<Item = Self::Transport, Error = io::Error>>;

    fn bind_transport(&self, io: T) -> Self::BindTransport {
        let read_buf = vec![0u8; 16 + 32 + 24 + 64];
        // create a future that will receive a handshake from a remote device.

        let atom_client = self.atom_client.clone();
        let now = ::std::time::Instant::now();
        let handshake_future = self.atom_client
        .state(StateAction::read("self.device"))
            .and_then(|trans| trans.first().unwrap().get_data::<DeviceManifest>())
            .and_then(|local_device| {
                let id = local_device.id;
                tokio_core::io::write_all(io, *id.as_bytes())
                .map(move |(socket, _)| (socket, local_device))
                .map_err(err::Error::from)
            })
            .and_then(|(socket, local_device)| {
                tokio_core::io::read_exact(socket, read_buf)
                    .map(move |(socket, buf)| (socket, buf, local_device))
                    .map_err(err::Error::from)
            })
            .and_then(|(socket, buf, local_device)| {
                // split the handshake into the individual componenets
                let mut buf = EasyBuf::from(buf);
                let id = Uuid::from_bytes(buf.drain_to(16).as_ref())
                    .map_err(err::Error::from)?;
                Ok((socket, id, buf, local_device))
            })
            // TODO: @dgriffen - Add code to check if 'group' is initialized
            .and_then(move |(socket, id, buf, local_device)| {
                atom_client.state(StateAction::read(format!("group.devices.{}",id)))
                    .and_then(|trans| trans.first().unwrap().get_data::<DeviceManifest>())
                    .map(move |public_device| (socket, public_device, buf, local_device))
            })
            .and_then(|(socket, other, mut buf, local_device)| {
                let pkey = crypto::stream::PublicKey::from_bytes(buf.drain_to(32).as_ref())?;
                let nonce = crypto::stream::Nonce::from_bytes(buf.drain_to(24).as_ref())?;
                let encrypted_signature_buf = buf.drain_to(64);

                // verify the other device is who it claims to be
                let key = crypto::stream::SymmetricKey::new(&pkey, &local_device.cipher_key.private.unwrap());
                let mut cipher = crypto::stream::StreamCipher::new_precomputed(&key, &nonce);
                let signature = crypto::sign::Signature::from_bytes(&cipher.process(encrypted_signature_buf.as_ref()))?;
                let valid_device = signature.verify(&pkey, &other.sign_key.public);

                // pass the processing on to another future. I would prefer to error out here if
                // the signature could not be verified, but we need to send a reponse back in both
                // cases. This was the best solution I could come up with.
                Ok((socket, cipher, valid_device))
            })
            .and_then(|(socket, cipher, valid_device)| {

                // if the handshake succeeded, respond with success, otherwise failure.
                let handshake_response = if valid_device {
                    tokio_core::io::write_all(socket, b"success").map_err(err::Error::from)
                } else {
                    tokio_core::io::write_all(socket, b"failure").map_err(err::Error::from)
                };

                handshake_response.and_then(move |(socket, _)| {
                    if valid_device {
                        Ok((socket, cipher))
                    } else {
                        Err(err::Error::new(err::ErrKind::IO, "bad handshake"))
                    }
                })
            })
            .map(move |(socket, cipher)| {
                let codec = MessageCodec { cipher: cipher };

                debug!(target: log_target!("bind_transport()"), "handshake time: {:?}", now.elapsed());
                socket.framed(codec)
            })
            .map_err(io::Error::from);

        Box::new(handshake_future)
    }
}

#[allow(deprecated)]
impl<T: Io + 'static> ServerProto<T> for AnonProto {
    type Request = SimpleMessage;
    type RequestBody = Vec<u8>;
    type Response = SimpleMessage;
    type ResponseBody = Vec<u8>;
    type Error = io::Error;

    type Transport = Framed<T, MessageCodec>;
    type BindTransport = Box<Future<Item = Self::Transport, Error = io::Error>>;

    fn bind_transport(&self, io: T) -> Self::BindTransport {
        let read_buf = vec![0u8; 32 + 24];
        // create a future that will receive a handshake from a remote device.
        let handshake_future = future::result(crypto::stream::gen_keypair().map_err(err::Error::from))
            .and_then(|cipher_key| {
                          let pkey = cipher_key.public;
                          let skey = cipher_key.private.unwrap();
                          tokio_core::io::write_all(io, pkey)
                              .map(move |(socket, _)| (socket, skey))
                              .map_err(err::Error::from)
                      })
            .and_then(|(socket, skey)| {
                          tokio_core::io::read_exact(socket, read_buf)
                              .map(move |(socket, buf)| (socket, buf, skey))
                              .map_err(err::Error::from)
                      })
            .and_then(move |(socket, buf, skey)| {
                // split the handshake into the individual componenets
                let mut buf = EasyBuf::from(buf);
                let pkey = crypto::stream::PublicKey::from_bytes(buf.drain_to(32).as_ref())?;
                let nonce = crypto::stream::Nonce::from_bytes(buf.drain_to(24).as_ref())?;

                // verify the other device is who it claims to be
                let key = crypto::stream::SymmetricKey::new(&pkey, &skey);
                let cipher = crypto::stream::StreamCipher::new_precomputed(&key, &nonce);

                // pass the processing on to another future. I would prefer to error out here if
                // the signature could not be verified, but we need to send a reponse back in both
                // cases. This was the best solution I could come up with.
                Ok((socket, cipher))
            })
            .map(move |(socket, cipher)| {
                     let codec = MessageCodec { cipher: cipher };

                     socket.framed(codec)
                 })
            .map_err(io::Error::from);

        Box::new(handshake_future)
    }
}
