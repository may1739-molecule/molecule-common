use super::{AnonProto, MessageCodec};
use super::{MessageProto, NodeProto};
use crypto;
use err;
use futures::Future;
use manifest::DeviceManifest;
use message::SimpleMessage;
use state::action::StateAction;
use std::io;
use tokio_core;
#[allow(deprecated)]
use tokio_core::io::{Framed, Io};
use tokio_proto::streaming::multiplex::ClientProto;
use uuid::Uuid;


#[allow(deprecated)]
impl<T: Io + 'static> ClientProto<T> for NodeProto {
    type Request = SimpleMessage;
    type RequestBody = Vec<u8>;
    type Response = SimpleMessage;
    type ResponseBody = Vec<u8>;
    type Error = io::Error;

    type Transport = Framed<T, MessageCodec>;
    type BindTransport = Box<Future<Item = Self::Transport, Error = io::Error>>;

    fn bind_transport(&self, io_t: T) -> Self::BindTransport {
        let atom_client = self.atom_client.clone();
        let atom_client_2 = self.atom_client.clone();
        let handshake_future = atom_client
            .state(StateAction::read("self.device"))
            .and_then(|trans| trans.first().unwrap().get_data::<DeviceManifest>())
            .map_err(io::Error::from)
            .and_then(move |local_device| {
                          let id = Vec::from(local_device.id.as_bytes().as_ref());
                          tokio_core::io::write_all(io_t, id)
                      })
            .and_then(|(socket, _)| tokio_core::io::read_exact(socket, [0u8; 16]))
            .map_err(err::Error::from)
            .and_then(|(socket, buf)| {
                          Uuid::from_bytes(&buf)
                              .map(|id| (socket, id))
                              .map_err(err::Error::from)
                      })
            .and_then(move |(socket, id)| {
                          atom_client
                              .state(StateAction::read(format!("group.devices.{}", id)))
                              .map(|trans| trans.first().unwrap().get_data::<DeviceManifest>())
                              .map(move |device| (socket, device))
                      })
            .map_err(io::Error::from)
            .and_then(move |(socket, device)| if let Ok(_) = device {
                          let proto = MessageProto { atom_client: atom_client_2 };
                          proto.bind_transport(socket)
                      } else {
                          let proto = AnonProto;
                          proto.bind_transport(socket)
                      });

        Box::new(handshake_future)
    }
}
#[allow(deprecated)]
impl<T: Io + 'static> ClientProto<T> for MessageProto {
    type Request = SimpleMessage;
    type RequestBody = Vec<u8>;
    type Response = SimpleMessage;
    type ResponseBody = Vec<u8>;
    type Error = io::Error;

    type Transport = Framed<T, MessageCodec>;
    type BindTransport = Box<Future<Item = Self::Transport, Error = io::Error>>;

    fn bind_transport(&self, io_t: T) -> Self::BindTransport {
        // create a future to send the handshake components in the following order: our
        // device id, ephemeral public key, nonce, payload
        let atom_client = self.atom_client.clone();
        let handshake_future = self.atom_client
            .state(StateAction::read("self.device"))
            .and_then(|trans| trans.first().unwrap().get_data::<DeviceManifest>())
            .and_then(|local_device| {
                tokio_core::io::read_exact(io_t, [0u8; 16])
                    .map(move |(socket, buf)| (socket, buf, local_device))
                    .map_err(err::Error::from)
            })
            .and_then(|(socket, buf, local_device)| {
                Uuid::from_bytes(&buf)
                    .map(|id| (socket, local_device, id))
                    .map_err(err::Error::from)
            })
            .and_then(move |(socket, local_device, id)| {
                atom_client.state(StateAction::read(format!("group.devices.{}", id)))
                    .and_then(|trans| trans.first().unwrap().get_data::<DeviceManifest>())
                    .map(move |device| (socket, local_device, device))
            })
            // at this point there is one of two outcomes, either the device was registered in our
            // state, in which case we proceed with the normal handshake. Or it wasn't in which
            // case we start ephemeral communication

            .and_then(move |(socket, local_device, device)| {

                // generate the cryptographic elements we will need for the handshake
                let local_cipher = crypto::stream::gen_keypair()?;
                let pkey = local_cipher.public;
                let skey = local_cipher.private.unwrap();
                let cipher_key = device.cipher_key.public;
                let key = crypto::stream::SymmetricKey::new(&cipher_key, &skey);
                let nonce = crypto::stream::Nonce::gen()?;

                // generate the components of the handshake
                let mut cipher = crypto::stream::StreamCipher::new_precomputed(&key, &nonce);
                let signature = crypto::sign::Signature::sign(&pkey, &local_device.sign_key.private.unwrap());
                let mut encrypted_payload = cipher.process(&signature);
                let mut handshake_payload = Vec::from(local_device.id.as_bytes().as_ref());
                handshake_payload.extend_from_slice(&pkey);
                handshake_payload.extend_from_slice(&nonce);
                handshake_payload.append(&mut encrypted_payload);

                Ok((socket, cipher, handshake_payload))
            })
            .and_then(|(socket, cipher, payload)| {
                tokio_core::io::write_all(socket, payload)
                    .map(move |(socket, _)| (socket, cipher))
                    .map_err(err::Error::from)
            })
            .and_then(|(socket, cipher)| {
                tokio_core::io::read_exact(socket, [0u8; 7])
                    .map(move |(socket, buf)| (socket, cipher, buf))
                    .map_err(err::Error::from)
            })
            .and_then(|(socket, cipher, buf)| if &buf == b"success" {
                Ok((socket, cipher))
            } else {
                Err(err::Error::new(err::ErrKind::IO, "bad handshake"))
            })
            .map(|(socket, cipher)| {
                let codec = MessageCodec { cipher: cipher };

                socket.framed(codec)
            })
            .map_err(io::Error::from);

        Box::new(handshake_future)
    }
}

#[allow(deprecated)]
impl<T: Io + 'static> ClientProto<T> for AnonProto {
    type Request = SimpleMessage;
    type RequestBody = Vec<u8>;
    type Response = SimpleMessage;
    type ResponseBody = Vec<u8>;
    type Error = io::Error;

    type Transport = Framed<T, MessageCodec>;
    type BindTransport = Box<Future<Item = Self::Transport, Error = io::Error>>;

    fn bind_transport(&self, io: T) -> Self::BindTransport {
        // create a future to send the handshake components in the following order: our
        // device id, ephemeral public key, nonce, payload
        let handshake_future = tokio_core::io::read_exact(io, [0u8; 32])
            .map_err(err::Error::from)
            .and_then(move |(socket, buf)| {

                // generate the cryptographic elements we will need for the handshake
                let server_pkey = crypto::stream::PublicKey::from_bytes(&buf)?;
                let cipher_key = crypto::stream::gen_keypair()?;
                let pkey = cipher_key.public;
                let skey = cipher_key.private.unwrap();

                let key = crypto::stream::SymmetricKey::new(&server_pkey, &skey);
                let nonce = crypto::stream::Nonce::gen()?;

                // generate the components of the handshake
                let cipher = crypto::stream::StreamCipher::new_precomputed(&key, &nonce);
                let mut handshake_payload = Vec::from(pkey.as_ref());
                handshake_payload.extend_from_slice(&nonce);

                Ok((socket, cipher, handshake_payload))
            })
            .and_then(|(socket, cipher, payload)| {
                          tokio_core::io::write_all(socket, payload)
                              .map(move |(socket, _)| (socket, cipher))
                              .map_err(err::Error::from)
                      })
            .map(|(socket, cipher)| {
                     let codec = MessageCodec { cipher: cipher };

                     socket.framed(codec)
                 })
            .map_err(io::Error::from);

        Box::new(handshake_future)
    }
}
