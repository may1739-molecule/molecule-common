use bincode::{SizeLimit, deserialize, serialize};
use byteorder::{ByteOrder, LittleEndian};
use crypto;
use message::{Message, MessageType, SimpleMessage};
use networking::AtomClient;
use std::error::Error;
use std::io;
use std::sync::Arc;
#[allow(deprecated)]
use tokio_core::io::{Codec, EasyBuf};
use tokio_proto::streaming;
use tokio_proto::streaming::multiplex::Frame;

mod client;
mod server;

/// The message codec defines the codec for transporting Messages across the wire.
/// All frames when serialized consist of a 13 byte header followed by a variable length
/// payload
///
/// ```text
/// +--------------------------------+--------+
/// |          Length (32)           |Type (8)|
/// +--------------------------------+--------+------------------------+
/// |                         Request Id (64)                          |
/// +==================================================================+
/// |                           Payload(...)                           |
/// +------------------------------------------------------------------+
/// ```
///
/// `Length`: The length of the `Payload` in bytes expressed as an unsigned 32-bit integer.
///           The 13 bytes of the header are not included in this value.
///
/// `Type`: The type of the frame expressed as an 8-bit unsigned integer. Presently the frame
///         type can be one of three values:
///
/// | Value  | Enum Variant  |
/// | ------ |---------------|
/// | 0      | `Message`     |
/// | 1      | `Body`        |
/// | 2      | `Error`       |
/// `Request Id`: The unique id associating this frame to its stream
///
/// `Payload`: The structure of the payload is dependent on the frame type.
pub struct MessageCodec {
    cipher: crypto::stream::StreamCipher,
}

/// Enum representation of the different message frame types. This type gets serialized as an 8-bit
/// unsigned integer by `MessageCodec`
enum MessageCodecType {
    Message,
    Body,
    Error,
}

impl MessageCodecType {
    /// Convert a `u8` into a `MessageCodecType`. This function will return an `Err` if src > 2.
    fn from_u8(src: u8) -> Result<MessageCodecType, ()> {
        match src {
            0 => Ok(MessageCodecType::Message),
            1 => Ok(MessageCodecType::Body),
            2 => Ok(MessageCodecType::Error),
            _ => Err(()),
        }
    }
}

impl From<MessageCodecType> for u8 {
    fn from(src: MessageCodecType) -> u8 {
        match src {
            MessageCodecType::Message => 0,
            MessageCodecType::Body => 1,
            MessageCodecType::Error => 2,
        }
    }
}

/// Type alias for convienince when dealing with protocols.
pub type WireMessage = streaming::Message<SimpleMessage, streaming::Body<Vec<u8>, io::Error>>;

impl From<WireMessage> for Message {
    fn from(src: WireMessage) -> Self {
        match src {
            streaming::Message::WithoutBody(simple_message) => Message::from(simple_message),
            streaming::Message::WithBody(simple_message, body) => {
                let msg = Message::from(simple_message);
                msg.set_stream(body.into())
            }
        }
    }
}

impl From<Message> for WireMessage {
    fn from(mut src: Message) -> Self {
        let stream = src.get_stream();
        let msg_type = src.get_msg_type().clone();
        let simple = SimpleMessage::from(src);
        match msg_type {
            MessageType::Stream => streaming::Message::WithBody(simple, stream.unwrap().into()),
            MessageType::Packet => streaming::Message::WithoutBody(simple),
            MessageType::Synchronized => streaming::Message::WithoutBody(simple),
        }
    }
}

#[allow(deprecated)]
impl Codec for MessageCodec {
    type In = Frame<SimpleMessage, Vec<u8>, io::Error>;
    type Out = Frame<SimpleMessage, Vec<u8>, io::Error>;

    fn decode(&mut self, buf: &mut EasyBuf) -> io::Result<Option<Self::In>> {
        // check to see if we have the entire header yet
        if buf.len() < 13 {
            return Ok(None);
        }

        // create a temporary cipher, if we dont have the whole frame yet we need to revert to the previous state
        let mut temp_cipher = self.cipher.clone();
        let header = temp_cipher.process(&buf.as_ref()[0..13]);

        let length = LittleEndian::read_u32(&header[0..4]);
        if buf.as_ref()[13..].len() < length as usize {
            return Ok(None);
        }
        self.cipher = temp_cipher;

        let frame_type =
            MessageCodecType::from_u8(header[4])
                .map_err(|_| io::Error::new(io::ErrorKind::InvalidData, "Invalid frame type"))?;
        let id = LittleEndian::read_u64(&header[5..13]);
        buf.drain_to(13);
        let payload = self.cipher
            .process(buf.drain_to(length as usize).as_ref());
        match frame_type {
            MessageCodecType::Message => {
                let simple_message: SimpleMessage =
                    deserialize::<SimpleMessage>(&payload)
                        .map_err(|_| io::Error::new(io::ErrorKind::Other, "Invalid message"))?;
                let body = match simple_message.msg_type {
                    MessageType::Stream => true,
                    _ => false,
                };

                Ok(Some(Frame::Message {
                            id: id,
                            message: simple_message,
                            body: body,
                            solo: false,
                        }))
            }
            MessageCodecType::Body => {
                let chunk = if length == 0 { None } else { Some(payload) };

                Ok(Some(Frame::Body {
                            id: id,
                            chunk: chunk,
                        }))
            }
            MessageCodecType::Error => {
                Ok(Some(Frame::Error {
                            id: id,
                            error: io::ErrorKind::Other.into(),
                        }))
            }
        }
    }

    fn encode(&mut self, msg: Self::Out, buf: &mut Vec<u8>) -> io::Result<()> {
        let (frame_type, id, mut payload_buf) = match msg {
            Frame::Message { id, message, .. } => {
                let payload =
                    serialize(&message, SizeLimit::Infinite)
                        .map_err(|_| io::Error::new(io::ErrorKind::Other, "serialization failed"))?;

                (MessageCodecType::Message, id, payload)
            }
            Frame::Body { id, chunk } => {
                let payload = chunk.unwrap_or_else(Vec::new);
                (MessageCodecType::Body, id, payload)
            }
            Frame::Error { id, error } => {
                let payload = error.description().as_bytes().to_vec();
                (MessageCodecType::Error, id, payload)
            }
        };

        let mut frame_buf = vec![0u8; 13];
        // write bytes 0-3
        LittleEndian::write_u32(&mut frame_buf[0..], payload_buf.len() as u32);

        // write byte 4
        frame_buf[4] = frame_type.into();

        // write bytes 5-12
        LittleEndian::write_u64(&mut frame_buf[5..], id);
        frame_buf.append(&mut payload_buf);

        buf.resize(frame_buf.len(), 0);
        self.cipher.process_with_buf(&frame_buf, buf);

        Ok(())
    }
}

#[derive(Copy, Clone)]
struct CryptoCodec {
    cipher: crypto::stream::StreamCipher,
}

#[allow(deprecated)]
impl Codec for CryptoCodec {
    type In = Vec<u8>;
    type Out = Vec<u8>;

    fn decode(&mut self, buf: &mut EasyBuf) -> io::Result<Option<Self::In>> {
        let ret = Ok(Some(self.cipher.process(buf.as_ref())));
        ret
    }

    fn encode(&mut self, msg: Self::Out, buf: &mut Vec<u8>) -> io::Result<()> {
        buf.resize(msg.len(), 0);
        self.cipher.process_with_buf(&msg, buf);

        Ok(())
    }
}

pub struct NodeProto {
    pub atom_client: Arc<AtomClient>,
}

/// The message protocol defines the procedure for connections between clients and servers.
/// The protocol is split into two halves, a client protocol and server protocol. Both
/// halves of the protocol rely on a database of authorized devices in order to establish
/// secure authenticated communication.
///
/// # Client Protocol
/// The client begins the handshake by first establishing a connection with the server. The
/// client then waits to recieve a 16-byte device id from the server which uniquely identifies
/// the device the client is connecting to. The client uses the id to determine the cryptographic
/// public keys of the server. Once the client has this information it constructs a payload to
/// send to the server.
///
/// ```text
/// +--------------------+----------------------------------------+
/// |   Device Id (16)   |       Ephemeral Public Key (32)        |
/// +--------------------+----------+-----------------------------+
/// |          Nonce (24)           |
/// +-------------------------------+--------------------------------------------------+
/// |                     Encrypted Public Key Signature (64)                          |
/// +----------------------------------------------------------------------------------+
/// ```
///
/// * `Device Id`: The unique id of the client device expressed as an array of 16 unsigned 8-bit
///                integers
///
/// * `Ephemeral Public Key`: The public half of the ephemeral keypair generated for this
///                           connection expressed as an array of 32 unsigned 8-bit integers
///
/// * `Nonce`: The random nonce generated for this connection expressed as an array of 24 unsigned
///            8-bit integers.
///
/// * `Encrypted Public Key Signature`: The ephemeral public key signature. As signed by the
///                                     client's signing keypair. This is used by the server to
///                                     verify the client's identity. This component is expresses
///                                     as an array of 64 unsigned 8-bit integers.
///
/// Once the payload has been sent the client waits for a response from the server. The server
/// responds with the status of the handshake.
///
/// # Server Protocol
/// When the server recieves a connection from a client it first sends back its 16-byte device id
/// in order to identify itself to the client. It then waits to recieve the contents of the payload
/// specified above. Once it receives the payload it verifies the signature against the
/// clients public key. If verification is successful the server responds with a success code.
/// Otherwise a failure code is returned and the connection aborted.
pub struct MessageProto {
    pub atom_client: Arc<AtomClient>,
}

/// The message protocol defines the procedure for connections between clients and servers that do
/// not have authentication information for each other. The protocol is split into two halves, a
/// client protocol and server protocol.
///
/// # Client Protocol
/// The client begins the handshake by first establishing a connection with the server. The
/// client then waits to recieve a 32-byte public key from the server which is used to generate
/// a symmetric key for encrypted communications. Once the client has this information it
/// constructs a payload to send to the server.
///
/// ```text
/// +----------------------------------------+
/// |       Ephemeral Public Key (32)        |
/// +----------------------------------------+
/// |          Nonce (24)           |
/// +-------------------------------+
/// ```
///
/// * `Ephemeral Public Key`: The public half of the ephemeral keypair generated for this
///                           connection expressed as an array of 32 unsigned 8-bit integers
///
/// * `Nonce`: The random nonce generated for this connection expressed as an array of 24 unsigned
///            8-bit integers.
///
/// Since the protocol does not authenticate clients or servers the handshake is assumed to have
/// have suceeded once the payload is sent.
///
/// # Server Protocol
/// When the server recieves a connection from a client it first generates an ephemeral key pair.
/// Once the keypair is generated the server sends the 32-byte public key to the client. The server
/// then waits to recieve the contents of the payload specified above. Once it receives the payload
/// it combined the received public key with its ephemeral secret key to establish a symmetric key
/// and secure the connection.
pub struct AnonProto;
