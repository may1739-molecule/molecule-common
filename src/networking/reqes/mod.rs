use err::*;
use futures::{Future, future};
use futures::stream::{BoxStream, Stream};
use futures::sync::{mpsc, oneshot};
use std::fmt;
use tokio_core::reactor::Handle;

#[derive(Debug)]
pub struct ReqesClient<M> {
    tx: mpsc::UnboundedSender<Request<M>>,
}

impl<M> Clone for ReqesClient<M>
    where M: 'static
{
    fn clone(&self) -> Self {
        ReqesClient { tx: self.tx.clone() }
    }
}

impl<M> ReqesClient<M>
    where M: 'static
{
    pub fn send(&self, data: M) -> Box<Future<Item = M, Error = Error>> {
        let (tx, rx) = oneshot::channel::<Result<M>>();
        let send = self.tx
            .clone()
            .send(Request {
                      tx: tx,
                      data: Some(data),
                  })
            .map_err(eif!(Error::from));
        let send = match send {
            Ok(d) => future::ok(d),
            Err(e) => future::err(e),
        };
        let send = send.and_then(|_| {
            rx.map_err(eif!(Error::from))
                .and_then(|res| {
                              let res = match res {
                                  Ok(d) => future::ok(d),
                                  Err(e) => future::err(e),
                              };
                              Box::new(res)
                          })
        });
        Box::new(send)
    }
}

#[derive(Debug)]
pub struct ReqesServer<M> {
    rx: mpsc::UnboundedReceiver<Request<M>>,
}

impl<M> ReqesServer<M>
    where M: 'static + Send
{
    pub fn receive(self) -> BoxStream<Request<M>, Error> {
        Box::new(self.rx
                     .map_err(eif!(|_| Error::new(ErrKind::IO, "ReqesServer Borken Pipe"))))
    }
}

#[derive(Debug)]
pub struct Request<M> {
    tx: oneshot::Sender<Result<M>>,
    pub data: Option<M>,
}

impl<M> Request<M>
    where M: 'static
{
    pub fn reply<F, E>(self, handle: &Handle, f: F)
        where F: Future<Item = M, Error = E> + 'static,
              E: fmt::Debug,
              Error: From<E>
    {
        handle.spawn(f.map_err(eif!(Error::from))
                         .then(move |res| {
                                   self.tx.send(res).unwrap_or(());
                                   future::ok(())
                               }));
    }
    pub fn take_data(&mut self) -> Option<M> {
        self.data.take()
    }
}

#[derive(Debug)]
pub struct Reqes<M> {
    pub client: ReqesClient<M>,
    pub server: ReqesServer<M>,
}

pub fn reqes<M>() -> Reqes<M> {
    let (tx, rx) = mpsc::unbounded();
    Reqes {
        client: ReqesClient { tx: tx },
        server: ReqesServer { rx: rx },
    }
}
