use super::SingleResponseFuture;
use futures::{Future, future};
use manifest::DeviceManifest;
use message::{Message, RoutingType, Signature, SimpleMessage};
use networking::AtomClient;
use networking::proto::{NodeProto, WireMessage};
use state::BStateFuture;
use state::action::StateAction;
use state::record::*;
use std::collections::HashSet;
use std::io;
use std::net::SocketAddr;
use std::str::FromStr;
use std::sync::Arc;
use tokio_core::reactor::Handle;
use tokio_proto::{TcpClient, TcpServer};
use tokio_service::{NewService, Service};
use uuid::Uuid;

#[derive(Clone, Debug)]
pub struct NetClient {
    atom_client: Arc<AtomClient>,
    handle: Handle,
}

impl NetClient {
    pub fn new(atom_client: Arc<AtomClient>, handle: &Handle) -> NetClient {
        NetClient {
            atom_client: atom_client,
            handle: handle.clone(),
        }
    }

    pub fn send_message(&self, message: Message) -> SingleResponseFuture {
        use message::RoutingType::*;

        let device_sig = message.get_destination().device.clone();
        let particle_sig = message.get_destination().particle.clone();
        match (device_sig, particle_sig) {
            (Any, One(ref id)) => self.message_particle(id, message),
            (One(ref device_id), One(_)) => self.one_one_message(device_id, message),
            (All, All) => self.all_all_message(message.into()),
            (All, One(ref id)) => self.all_one_message(id, message.into()),
            _ => unimplemented!(),
        }
    }

    pub fn message_particle(&self, particle: &Uuid, message: Message) -> SingleResponseFuture {
        debug!("send message to particle");
        let atom_client = self.atom_client.clone();
        let atom_client_2 = self.atom_client.clone();
        let handle = self.handle.clone();
        let node_iter = atom_client.state(StateAction::read(format!("group.particles.{}", *particle)))
                .map(|result: Transaction| -> HashSet<String> {
                    result.first().unwrap().children
                })
                .and_then(move |device_uuid_set: HashSet<String>| -> BStateFuture {
                    let atom_client = atom_client.clone();
                    let transaction: Vec<StateAction> = device_uuid_set.into_iter()
                        .map(|key| StateAction::read(format!("group.devices.{}", Record::get_name(key))))
                        .collect();
                    atom_client.state(StateAction::from(transaction))
                })
                .map(|results: Transaction| {
                    Vec::from(results)
                        .into_iter()
                        .map(|rec| rec.get_data::<DeviceManifest>())
                        .filter_map(Result::ok)
                })
                .map(move |devices| {
                    let atom_client = atom_client_2;
                    devices.map(move |device: DeviceManifest| {
                        TcpClient::new(NodeProto { atom_client: atom_client.clone() })
                            .connect(&device.address, &handle)
                    })
                })
                .map_err(eif!(io::Error::from))
                //TODO: future::select_ok will panic if a device is not found for a particle i.e. connections is empty
                //And detection code to avoid
                .and_then(|connections| future::select_ok(connections));

        let client_future =
            node_iter.and_then(|service| {
                                   let wire_message: WireMessage = message.into();
                                   service.0.call(wire_message).map(|msg| msg.into())
                               });

        SingleResponseFuture { inner: Box::new(client_future) }
    }

    fn one_one_message(&self, device: &Uuid, mut message: Message) -> SingleResponseFuture {
        //Resolve Device Routing
        let p = message.get_destination().particle.clone();
        message.set_destination(Signature::new(RoutingType::One(device.clone()), p));
        let handle = self.handle.clone();
        let atom_client = self.atom_client.clone();
        let maybe_addr = message.get_data::<SocketAddr>();
        let client_future = self.atom_client
            .state(StateAction::read(format!("group.devices.{}", device)))
            .and_then(|results| {
                          results
                              .first().unwrap()
                              .get_data::<DeviceManifest>()
                              .map(|device| device.address)
                      })
            .or_else(move |_| {
                         // There was an error, must mean the device doesn't exist yet. Get the address from the message
                         maybe_addr
                     })
            .map_err(eif!(io::Error::from))
            .and_then(move |address| TcpClient::new(NodeProto { atom_client: atom_client }).connect(&address, &handle))
            .and_then(|service| {
                          let wire_message: WireMessage = message.into();
                          service.call(wire_message).map(|msg| msg.into())
                      });

        SingleResponseFuture { inner: Box::new(client_future) }
    }

    fn all_all_message(&self, message: SimpleMessage) -> SingleResponseFuture {
        // get all particles that can do an action
        let c_self = self.clone();
        let response_future = self.atom_client
            .state(StateAction::read(format!("group.actions.{}.receive", message.action)))
            .and_then(move |results| {
                let response_holder = Message::from(message.clone())
                    .response()
                    .trace("", Uuid::nil());
                let future_vec = results
                    .first()
                    .unwrap()
                    .children
                    .into_iter()
                    .map(|key| Record::get_name(key))
                    .map(|particle_string| Uuid::from_str(&particle_string).unwrap())
                    .map(move |particle| {
                             c_self.all_one_message(&particle, message.clone().into())
                            .map(|message| {
                                message.get_data::<Vec<Result<SimpleMessage, ::err::Error>>>()
                                    .unwrap()
                            })
                            .then(|result| Ok(result))
                         });

                future::join_all(future_vec).map(move |results_vec| {
                    let data = results_vec
                        .into_iter()
                        .flat_map(|result| match result {
                                      Ok(res) => res,
                                      Err(err) => vec![Err(err.into())],
                                  })
                        .collect::<Vec<Result<SimpleMessage, ::err::Error>>>();

                    response_holder.data(Ok(data)).build_packet()
                })
            });

        SingleResponseFuture { inner: Box::new(response_future.from_err()) }
    }

    fn all_one_message(&self, particle: &Uuid, message: SimpleMessage) -> SingleResponseFuture {
        //Resolve Particle Routing
        let mut message: Message = message.into();
        let d = message.get_destination().device.clone();
        message.set_destination(Signature::new(d, RoutingType::One(particle.clone())));
        let message: SimpleMessage = message.into();

        let c_self = self.clone();
        let response_future = self.atom_client
            .state(StateAction::read(format!("group.particles.{}", particle)))
            .and_then(move |results| {
                let response_holder = Message::from(message.clone())
                    .response()
                    .trace("", Uuid::nil());
                let future_vec = results
                    .first()
                    .unwrap()
                    .children
                    .into_iter()
                    .map(|key| Record::get_name(key))
                    .map(|device_string| Uuid::from_str(&device_string).unwrap())
                    .map(move |device| {
                             c_self
                                 .one_one_message(&device, message.clone().into())
                                 .map(|message| SimpleMessage::from(message))
                                 .then(|result| Ok(result))
                         });

                // creates a future that will resolve to a vec of
                future::join_all(future_vec).map(move |results_vec| {
                    let data = results_vec
                        .into_iter()
                        .map(|result| match result {
                                 Ok(result) => Ok(result),
                                 Err(err) => Err(err.into()),
                             })
                        .collect::<Vec<Result<SimpleMessage, ::err::Error>>>();

                    response_holder.data(Ok(data)).build_packet()
                })
            });

        SingleResponseFuture { inner: Box::new(response_future.from_err()) }
    }
}

pub struct NetServer {
    atom_client: Arc<AtomClient>,
    addr: SocketAddr,
}

impl NetServer {
    pub fn new(addr: SocketAddr, atom_client: Arc<AtomClient>) -> NetServer {
        NetServer {
            atom_client: atom_client,
            addr: addr,
        }
    }

    pub fn serve<S>(&self, new_service: S)
        where S: NewService<Request = Message, Response = Message, Error = io::Error> + Send + Sync + 'static
    {
        let new_service = ServerTypeMap { inner: new_service };
        TcpServer::new(NodeProto { atom_client: self.atom_client.clone() },
                       self.addr)
                .serve(new_service)
    }
}

struct ServerTypeMap<S> {
    inner: S,
}

impl<S> Service for ServerTypeMap<S>
    where S: Service<Request = Message, Response = Message, Error = io::Error>,
          S::Future: 'static
{
    type Request = WireMessage;
    type Response = WireMessage;
    type Error = io::Error;
    type Future = Box<Future<Item = WireMessage, Error = io::Error>>;

    fn call(&self, req: Self::Request) -> Self::Future {
        Box::new(self.inner
                     .call(req.into())
                     .map(|res| WireMessage::from(res)))
    }
}

impl<S> NewService for ServerTypeMap<S>
    where S: NewService<Request = Message, Response = Message, Error = io::Error>,
          <S::Instance as Service>::Future: 'static
{
    type Request = WireMessage;
    type Response = WireMessage;
    type Error = io::Error;
    type Instance = ServerTypeMap<S::Instance>;

    fn new_service(&self) -> io::Result<Self::Instance> {
        let inner = self.inner.new_service()?;
        Ok(ServerTypeMap { inner: inner })
    }
}
