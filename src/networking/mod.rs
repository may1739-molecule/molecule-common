use futures::{self, Future, Poll, future};

use handler::AtomClient;
use message::Message;
use std::io;
use std::net::SocketAddr;
use std::path::PathBuf;
use std::sync::Arc;
use std::thread;
use tokio_core::reactor::Handle;
use tokio_service::{NewService, Service};

mod lan;
mod ipc;
mod proto;
pub mod reqes;

use self::lan::{NetClient, NetServer};

pub trait Client {
    type Response;
    type Future: Future<Item = Self::Response, Error = io::Error>;
    fn request(&self, req: Message) -> Self::Future;
}

pub trait Server {
    fn serve<S>(&self, new_service: S) -> Result<thread::JoinHandle<()>, ()>
        where S: NewService<Request = Message, Response = Message, Error = io::Error> + Send + Sync + 'static;
}

#[derive(Debug)]
pub struct NetworkClient {
    net_client: NetClient,
    atom: Arc<AtomClient>,
}

impl NetworkClient {
    pub fn new(atom: AtomClient, handle: &Handle) -> NetworkClient {
        let atom = Arc::new(atom);
        NetworkClient {
            net_client: NetClient::new(atom.clone(), handle),
            atom: atom,
        }
    }
}

impl Client for NetworkClient {
    type Response = Message;
    type Future = SingleResponseFuture;

    fn request(&self, req: Message) -> Self::Future {
        self.net_client.send_message(req)
    }
}

pub struct NetworkServer {
    atom: AtomClient,
    addr: SocketAddr,
}

impl NetworkServer {
    pub fn new(atom: AtomClient, addr: SocketAddr) -> NetworkServer {
        NetworkServer {
            atom: atom,
            addr: addr,
        }
    }

    pub fn forwarder(self) {
        let atom = self.atom.clone();
        self.serve(move || Ok(NetPassthrough { atom: atom.clone() }))
            .unwrap();
    }
}

impl Server for NetworkServer {
    fn serve<S>(&self, new_service: S) -> Result<thread::JoinHandle<()>, ()>
        where S: NewService<Request = Message, Response = Message, Error = io::Error> + Send + Sync + 'static
    {
        if new_service.new_service().is_ok() {
            let addr = self.addr;
            let atom = Arc::new(self.atom.clone());
            let th = thread::spawn(move || {
                NetServer::new(addr, atom).serve(new_service);
                println!("Woops! NetworkServer stopped working");
            });
            Ok(th)
        } else {
            Err(())
        }
    }
}

struct NetPassthrough {
    atom: AtomClient,
}
impl Service for NetPassthrough {
    type Request = Message;
    type Response = Message;
    type Error = io::Error;
    type Future = Box<Future<Item = Message, Error = io::Error>>;

    fn call(&self, req: Self::Request) -> Self::Future {

        let (ret_tx, ret_rx) = futures::sync::oneshot::channel();
        let atom = self.atom.clone();
        thread::spawn(move || {
            atom.forward(req)
                .then(|response| match ret_tx.send(response) {
                    Ok(_) => future::ok(()),
                    Err(_) => future::err(()),
                })
                .wait()
                .unwrap();
        });

        Box::new(ret_rx.map_err(|e| io::Error::new(io::ErrorKind::Other, e))
            .and_then(|res| match res {
                Ok(d) => future::ok(d),
                Err(e) => future::err(e),
            }))
    }
}



#[derive(Debug)]
pub struct SocketClient {
    handle: Handle,
    path: PathBuf,
}

impl SocketClient {
    pub fn new(handle: &Handle, path: PathBuf) -> SocketClient {
        SocketClient {
            handle: handle.clone(),
            path: path,
        }
    }
}

impl Client for SocketClient {
    type Response = Message;
    type Future = Box<Future<Item = Message, Error = io::Error>>;

    fn request(&self, req: Message) -> Self::Future {
        Box::new(future::result(ipc::IpcClient::new(&self.path, &self.handle))
            .and_then(|client| client.send_message(req)))
    }
}

pub struct SocketServer {
    path: PathBuf,
}

impl SocketServer {
    pub fn new(path: PathBuf) -> SocketServer {
        SocketServer { path: path }
    }
}

impl Server for SocketServer {
    fn serve<S>(&self, new_service: S) -> Result<thread::JoinHandle<()>, ()>
        where S: NewService<Request = Message, Response = Message, Error = io::Error> + Send + Sync + 'static
    {
        let path = self.path.clone();
        if new_service.new_service().is_ok() {
            let th = thread::spawn(move || {
                ipc::IpcServer::new(path).serve(new_service);
                println!("Woops! SocketServer stooped working");
            });
            Ok(th)
        } else {
            Err(())
        }
    }
}

pub struct SingleResponseFuture {
    inner: Box<Future<Item = Message, Error = io::Error>>,
}

impl Future for SingleResponseFuture {
    type Item = Message;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        self.inner.poll()
    }
}
