use super::SingleResponseFuture;
use futures::Future;
use message::Message;
use networking::proto::{AnonProto, WireMessage};
use std::io;
use std::path::{Path, PathBuf};
use tokio_core::reactor::Handle;
use tokio_proto::util::client_proxy::ClientProxy;
use tokio_service::{NewService, Service};
use tokio_uds_proto::{UnixClient, UnixServer};

pub struct IpcClient {
    inner: ClientTypeMap<ClientProxy<WireMessage, WireMessage, io::Error>>,
}

impl IpcClient {
    pub fn new<P>(path: P, handle: &Handle) -> io::Result<IpcClient>
        where P: AsRef<Path>
    {
        let inner = UnixClient::new(AnonProto)
            .connect(path, handle)
            .map(|proxy| ClientTypeMap { inner: proxy })?;
        Ok(IpcClient { inner: inner })
    }

    pub fn send_message(&self, message: Message) -> SingleResponseFuture {
        SingleResponseFuture { inner: self.inner.call(message) }
    }
}

struct ClientTypeMap<T> {
    inner: T,
}

impl<T> Service for ClientTypeMap<T>
    where T: Service<Request = WireMessage, Response = WireMessage, Error = io::Error>,
          T::Future: 'static
{
    type Request = Message;
    type Response = Message;
    type Error = io::Error;
    type Future = Box<Future<Item = Message, Error = io::Error>>;

    fn call(&self, req: Message) -> Self::Future {
        Box::new(self.inner.call(req.into()).map(Message::from))
    }
}

pub struct IpcServer {
    path: PathBuf,
}

impl IpcServer {
    pub fn new(path: PathBuf) -> IpcServer {
        IpcServer { path: path }
    }

    pub fn serve<S>(&self, new_service: S)
        where S: NewService<Request = Message, Response = Message, Error = io::Error> + Send + Sync + 'static
    {
        let new_service = ServerTypeMap { inner: new_service };
        UnixServer::new(AnonProto, self.path.clone()).serve(new_service)
    }
}

struct ServerTypeMap<S> {
    inner: S,
}

impl<S> Service for ServerTypeMap<S>
    where S: Service<Request = Message, Response = Message, Error = io::Error>,
          S::Future: 'static
{
    type Request = WireMessage;
    type Response = WireMessage;
    type Error = io::Error;
    type Future = Box<Future<Item = WireMessage, Error = io::Error>>;

    fn call(&self, req: Self::Request) -> Self::Future {
        Box::new(self.inner
                     .call(req.into())
                     .map(|res| WireMessage::from(res)))
    }
}

impl<S> NewService for ServerTypeMap<S>
    where S: NewService<Request = Message, Response = Message, Error = io::Error>,
          <S::Instance as Service>::Future: 'static
{
    type Request = WireMessage;
    type Response = WireMessage;
    type Error = io::Error;
    type Instance = ServerTypeMap<S::Instance>;

    fn new_service(&self) -> io::Result<Self::Instance> {
        let inner = self.inner.new_service()?;
        Ok(ServerTypeMap { inner: inner })
    }
}
