use err;
use futures::Future;
use message::{Message, MessageBuilder, Signature};
use networking::reqes::*;
use state::BStateFuture;
use state::action::StateAction;
use std::io;
use uuid::Uuid;

pub trait Handler {
    fn handle(client: AtomClient,
              message: Message)
              -> Box<Future<Item = Message, Error = io::Error>>;
}

#[derive(Clone, Debug)]
pub struct AtomClient {
    trace: Uuid,
    client: ReqesClient<Message>,
    source: Signature,
    atom: Signature,
}

impl AtomClient {
    pub fn new(trace: Uuid,
               client: ReqesClient<Message>,
               source: Signature,
               atom: Signature)
               -> AtomClient {
        AtomClient {
            trace: trace,
            client: client,
            source: source,
            atom: atom,
        }
    }
    pub fn forward(&self, message: Message) -> Box<Future<Item = Message, Error = io::Error>> {
        Box::new(self.client.send(message).map_err(eif!(io::Error::from)))
    }
    pub fn state(&self, action: StateAction) -> BStateFuture {
        Box::new(self.client
                     .send(MessageBuilder::new("STATE_ACTION")
                               .source(self.source.clone())
                               .destination(self.atom.clone())
                               .data(&action)
                               .unwrap()
                               .trace("AtomClient.state()", self.trace)
                               .build_packet())
                     .map_err(err::Error::from)
                     .and_then(|res| res.get_data()))
    }
}
