use bincode;
use futures;
use r2d2;
use redis;
use serde_json;
use std::error;
use std::fmt;
use std::io;
use std::result;
use uuid;
use std::sync::PoisonError;

/// Message Related Errors produce an Error
pub type Result<T> = result::Result<T, Error>;

#[derive(Eq, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct Error {
    kind: ErrKind,
    description: String,
}

impl Error {
    pub fn new<S>(kind: ErrKind, description: S) -> Error
        where S: Into<String>
    {
        Error {
            kind: kind,
            description: description.into(),
        }
    }
    pub fn to_string(&self) -> String {
        format!("{}", self)
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        self.description.as_str()
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl From<Error> for io::Error {
    fn from(e: Error) -> Self {
        io::Error::new(io::ErrorKind::Other, e)
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        let s = format!("{}", e);
        match e.into_inner() {
            Some(e) => {
                match e.downcast::<Error>() {
                    Ok(my_err) => *my_err,
                    Err(other_err) => {
                        Error {
                            kind: ErrKind::IO,
                            description: format!("{}", other_err),
                        }
                    }
                }
            }
            None => {
                Error {
                    kind: ErrKind::IO,
                    description: s,
                }
            }
        }
    }
}

impl From<bincode::Error> for Error {
    fn from(e: bincode::Error) -> Self {
        Error {
            kind: ErrKind::Serialization,
            description: format!("{}", e),
        }
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error {
            kind: ErrKind::Serialization,
            description: format!("{}", e),
        }
    }
}

impl From<uuid::ParseError> for Error {
    fn from(e: uuid::ParseError) -> Self {
        Error {
            kind: ErrKind::Serialization,
            description: format!("{}", e),
        }
    }
}

impl From<redis::RedisError> for Error {
    fn from(e: redis::RedisError) -> Self {
        Error {
            kind: ErrKind::Redis,
            description: format!("{}", e),
        }
    }
}

impl From<r2d2::InitializationError> for Error {
    fn from(e: r2d2::InitializationError) -> Self {
        Error {
            kind: ErrKind::Redis,
            description: format!("{}", e),
        }
    }
}

impl From<r2d2::GetTimeout> for Error {
    fn from(e: r2d2::GetTimeout) -> Self {
        Error {
            kind: ErrKind::Redis,
            description: format!("{}", e),
        }
    }
}

impl<T> From<futures::sync::mpsc::SendError<T>> for Error
    where T: Sized
{
    fn from(e: futures::sync::mpsc::SendError<T>) -> Self {
        Error {
            kind: ErrKind::IO,
            description: format!("{}", e),
        }
    }
}

impl From<futures::Canceled> for Error {
    fn from(e: futures::Canceled) -> Self {
        Error {
            kind: ErrKind::IO,
            description: format!("{}", e),
        }
    }
}

impl<D> From<PoisonError<D>> for Error where D: fmt::Debug {
    fn from(e: PoisonError<D>) -> Self {
        Error {
            kind: ErrKind::Other,
            description: format!("Locking Error: {:?}", e)
        }
    }
}

impl From<()> for Error {
    fn from(_: ()) -> Self {
        Error {
            kind: ErrKind::Other,
            description: "()".into(),
        }
    }
}

impl From<String> for Error {
    fn from(s: String) -> Self {
        Error {
            kind: ErrKind::Other,
            description: s
        }
    }
}

impl From<&'static str> for Error {
    fn from(s: &'static str) -> Self {
        Error {
            kind: ErrKind::Other,
            description: s.into()
        }
    }
}


#[derive(Eq, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub enum ErrKind {
    Redis,
    State,
    Serialization,
    Permissions,
    Message,
    IO,
    Invariant,
    App,
    Other,
}
