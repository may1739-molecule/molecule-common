use err::*;
use futures::{Poll, Stream};
use futures::sync::mpsc::Sender;
use serde::{Deserialize, Serialize};
use serde_json;
use std::fmt;
use std::io;
use std::marker::PhantomData;
use tokio_proto::streaming::Body;
use uuid::Uuid;

#[derive(Eq, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct SimpleMessage {
    //Routing
    pub id: Uuid,
    pub routing: Option<Routing>,
    pub msg_type: MessageType,
    //Payload
    pub action: String,
    pub data: Result<String>,
    //Debug
    pub trace: Trace,
}

impl From<Message> for SimpleMessage {
    fn from(t: Message) -> Self {
        SimpleMessage {
            //Routing
            id: t.id,
            routing: t.routing,
            msg_type: t.msg_type,
            //Payload
            action: t.action,
            data: t.data,
            //Debug
            trace: t.trace,
        }
    }
}

impl From<SimpleMessage> for Message {
    fn from(t: SimpleMessage) -> Self {
        Message {
            //Routing
            id: t.id,
            routing: t.routing,
            msg_type: t.msg_type,
            //Payload
            action: t.action,
            data: t.data,
            stream: None,
            //Debug
            trace: t.trace,
        }
    }
}

pub struct Message {
    //Routing
    id: Uuid,
    routing: Option<Routing>,
    msg_type: MessageType,
    //Payload
    action: String,
    data: Result<String>,
    stream: Option<DataStream>,
    //Debug
    trace: Trace,
}

impl PartialEq for Message {
    fn eq(&self, other: &Message) -> bool {
        use self::MessageType::*;
        (self.id == other.id) & (self.routing == other.routing) &
        (match (&self.msg_type, &other.msg_type) {
             (&Packet, &Packet) => true,
             (&Synchronized, &Synchronized) => true,
             (&Stream, &Stream) => false,
             _ => false,
         }) & (self.action == other.action) & (self.data == other.data) &
        (self.trace == other.trace)
    }
}

impl fmt::Debug for Message {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Message {{\n")?;
        write!(f, "\tid: {}\n", self.id.hyphenated())?;
        write!(f, "\trouting: {:?}\n", self.routing)?;
        write!(f, "\tmsg_type: {:?}\n", self.msg_type)?;
        write!(f, "\taction: {}\n", self.action)?;
        write!(f, "\tdata: ")?;
        match self.data {
            Ok(ref data) => {
                write!(f, "Ok({})", data)?;
            }
            Err(ref err) => {
                write!(f, "Err({:?})", err)?;
            }
        }
        write!(f, "\n")?;
        write!(f, "\ttrace: {:?}\n", self.trace)?;
        write!(f, "}}")

    }
}

impl Message {
    pub fn get_id(&self) -> &Uuid {
        &self.id
    }
    pub fn get_routing(&self) -> &Routing {
        match &self.routing {
            &Some(ref r) => r,
            &None => panic!("illegal state, no routing information available"),
        }
    }
    pub fn get_destination(&self) -> &Signature {
        &self.get_routing().destination
    }
    pub fn get_source(&self) -> &Signature {
        &self.get_routing().source
    }
    pub fn get_msg_type(&self) -> &MessageType {
        &self.msg_type
    }
    pub fn get_action(&self) -> &str {
        &self.action
    }
    pub fn get_data<T>(&self) -> Result<T>
        where T: Deserialize
    {
        match self.data {
            Ok(ref data) => serde_json::from_str(data).map_err(Error::from),
            Err(ref e) => Err(e.clone()),
        }
    }
    pub fn get_raw(&self) -> &Result<String> {
        &self.data
    }
    pub fn get_trace(&mut self) -> &mut Trace {
        &mut self.trace
    }
    pub fn get_stream(&mut self) -> Option<DataStream> {
        match self.stream.take() {
            Some(stream) => Some(stream),
            None => None,
        }
    }
    pub fn set_source(&mut self, signature: Signature) {
        match self.routing.take() {
            Some(routing) => self.routing = Some(Routing::new(signature, routing.destination)),
            None => (),
        }
    }
    pub fn set_destination(&mut self, signature: Signature) {
        match self.routing.take() {
            Some(routing) => self.routing = Some(Routing::new(routing.source, signature)),
            None => (),
        }
    }
    pub fn set_stream(mut self, stream: DataStream) -> Self {
        self.stream = Some(stream);
        self
    }
    pub fn response(self) -> ResponseBuilder<(), ()> {
        ResponseBuilder {
            id: self.id,
            action: self.action,
            data: Err(Error::new(ErrKind::Message, "response data not set")),
            trace: self.trace,
            data_state: PhantomData,
            trace_state: PhantomData,
        }
    }
}

/// Message Routing Information
#[derive(Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct Routing {
    source: Signature,
    destination: Signature,
}

impl Routing {
    pub fn new(src: Signature, dst: Signature) -> Routing {
        Routing {
            source: src,
            destination: dst,
        }
    }
    pub fn get_destination(&self) -> &Signature {
        &self.destination
    }
    pub fn get_source(&self) -> &Signature {
        &self.source
    }
}

impl fmt::Debug for Routing {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "src: {:?}, dst: {:?}", self.source, self.destination)
    }
}

/// Device/Particle Signature
#[derive(Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct Signature {
    pub device: RoutingType,
    pub particle: RoutingType,
}

impl Signature {
    pub fn new(device: RoutingType, particle: RoutingType) -> Signature {
        Signature {
            device: device,
            particle: particle,
        }
    }
    pub fn broadcast() -> Signature {
        Signature {
            device: RoutingType::All,
            particle: RoutingType::All,
        }
    }
    pub fn atom(id: Uuid) -> Signature {
        Signature {
            device: RoutingType::One(id),
            particle: RoutingType::One(id),
        }
    }
}

impl fmt::Debug for Signature {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "{{ device: {:?} , particle: {:?} }}",
               self.device,
               self.particle)
    }
}

#[derive(Eq, PartialEq, Clone, Serialize, Deserialize)]
pub enum RoutingType {
    Any,
    All,
    Some(Vec<Uuid>),
    One(Uuid),
}

impl RoutingType {
    pub fn get_id(&self) -> Uuid {
        match self {
            &RoutingType::One(u) => u,
            _ => panic!("cannot get id"),
        }
    }
}

impl fmt::Debug for RoutingType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::RoutingType::*;
        match self {
            &Any => write!(f, "Any"),
            &All => write!(f, "All"),
            &Some(ref list) => {
                write!(f, "[\n")?;
                for u in list {
                    write!(f, "\t{}", u.hyphenated())?;
                }
                write!(f, "]")
            }
            &One(ref u) => write!(f, "{}", u.hyphenated()),
        }
    }
}

/// Message Type
#[derive(Eq, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub enum MessageType {
    Packet,
    Synchronized,
    Stream,
}

/// Debug Information for Message Tracing
#[derive(Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct Trace {
    trace: Vec<(String, Uuid)>,
}

impl Trace {
    pub fn new(trace: (String, Uuid)) -> Trace {
        Trace { trace: vec![trace] }
    }
    pub fn add<S>(&mut self, name: S, id: Uuid)
        where S: Into<String>
    {
        self.trace.push((name.into(), id));
    }
    pub fn ttl(&self) -> usize {
        self.trace.len()
    }
}

impl fmt::Debug for Trace {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[ ")?;
        for &(ref n, ref id) in &self.trace {
            write!(f, "({}\t{}) ", n, id.hyphenated())?;
        }
        write!(f, "]")
    }
}

/// Stream
#[derive(Debug)]
pub struct DataStream {
    inner: Body<Vec<u8>, io::Error>,
}

impl DataStream {
    pub fn pair() -> (Sender<io::Result<Vec<u8>>>, DataStream) {
        let (tx, rx) = Body::pair();
        (tx, DataStream { inner: rx })
    }
}

impl From<Body<Vec<u8>, io::Error>> for DataStream {
    fn from(from: Body<Vec<u8>, io::Error>) -> DataStream {
        DataStream { inner: from }
    }
}

impl From<DataStream> for Body<Vec<u8>, io::Error> {
    fn from(from: DataStream) -> Body<Vec<u8>, io::Error> {
        from.inner
    }
}

impl Stream for DataStream {
    type Item = Vec<u8>;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        self.inner.poll()
    }
}

#[derive(Debug)]
pub struct SourceSet;
#[derive(Debug)]
pub struct DestinationSet;
#[derive(Debug)]
pub struct DataSet;
#[derive(Debug)]
pub struct TraceSet;

pub struct ResponseBuilder<DataState, TraceState> {
    id: Uuid,
    action: String,
    data: Result<String>,
    trace: Trace,

    data_state: PhantomData<DataState>,
    trace_state: PhantomData<TraceState>,
}

impl<DataState> ResponseBuilder<DataState, ()> {
    pub fn trace<S>(mut self, name: S, id: Uuid) -> ResponseBuilder<DataState, TraceSet>
        where S: Into<String>
    {
        self.trace.add(name.into(), id);
        ResponseBuilder {
            id: self.id,
            action: self.action,
            data: self.data,
            trace: self.trace,

            data_state: PhantomData,
            trace_state: PhantomData,
        }
    }
}

impl<TraceState> ResponseBuilder<(), TraceState> {
    pub fn data<D>(self, data: Result<D>) -> ResponseBuilder<DataSet, TraceState>
        where D: Serialize + Sized
    {
        let data = match data {
            Ok(d) => serde_json::to_string(&d).map_err(Error::from),
            Err(e) => Err(e),
        };
        ResponseBuilder {
            id: self.id,
            action: self.action,
            data: data,
            trace: self.trace,

            data_state: PhantomData,
            trace_state: PhantomData,
        }
    }
}

impl ResponseBuilder<DataSet, TraceSet> {
    pub fn build_packet(self) -> Message {
        Message {
            //Routing
            id: self.id,
            routing: None,
            msg_type: MessageType::Packet,
            //Payload
            action: self.action,
            data: self.data,
            stream: None,
            //Debug
            trace: self.trace,
        }
    }
    pub fn build_sync(self) -> Message {
        Message {
            //Routing
            id: self.id,
            routing: None,
            msg_type: MessageType::Synchronized,
            //Payload
            action: self.action,
            data: self.data,
            stream: None,
            //Debug
            trace: self.trace,
        }
    }
    pub fn build_stream(self, stream: DataStream) -> Message {
        Message {
            //Routing
            id: self.id,
            routing: None,
            msg_type: MessageType::Stream,
            //Payload
            action: self.action,
            data: self.data,
            stream: Some(stream),
            //Debug
            trace: self.trace,
        }
    }
}

#[derive(Debug)]
pub struct MessageBuilder<SourceState, DestinationState, DataState, TraceState> {
    //Routing
    id: Uuid,
    source: Option<Signature>,
    destination: Option<Signature>,
    //Payload
    action: String,
    data: Option<String>,
    //Debug
    trace: Option<(String, Uuid)>,
    source_state: PhantomData<SourceState>,
    destination_state: PhantomData<DestinationState>,
    data_state: PhantomData<DataState>,
    trace_state: PhantomData<TraceState>,
}

impl MessageBuilder<(), (), (), ()> {
    pub fn new<T>(act: T) -> MessageBuilder<(), (), (), ()>
        where T: Into<String>
    {
        MessageBuilder {
            //Routing
            id: Uuid::new_v4(),
            source: None,
            destination: None,
            //Payload
            action: act.into(),
            data: None,
            //Debug
            trace: None,
            source_state: PhantomData,
            destination_state: PhantomData,
            data_state: PhantomData,
            trace_state: PhantomData,
        }
    }
}

impl<SourceState, DestinationState, DataState, TraceState> MessageBuilder<SourceState,
                                                                          DestinationState,
                                                                          DataState,
                                                                          TraceState> {
    pub fn source(self,
                  src: Signature)
                  -> MessageBuilder<SourceSet, DestinationState, DataState, TraceState> {
        MessageBuilder {
            //Routing
            id: self.id,
            source: Some(src),
            destination: self.destination,
            //Payload
            action: self.action,
            data: self.data,
            //Debug
            trace: self.trace,

            source_state: PhantomData,
            destination_state: PhantomData,
            data_state: PhantomData,
            trace_state: PhantomData,
        }
    }
    pub fn destination(self,
                       dst: Signature)
                       -> MessageBuilder<SourceState, DestinationSet, DataState, TraceState> {
        MessageBuilder {
            //Routing
            id: self.id,
            source: self.source,
            destination: Some(dst),
            //Payload
            action: self.action,
            data: self.data,
            //Debug
            trace: self.trace,

            source_state: PhantomData,
            destination_state: PhantomData,
            data_state: PhantomData,
            trace_state: PhantomData,
        }
    }

    pub fn trace<S>(self,
                    name: S,
                    id: Uuid)
                    -> MessageBuilder<SourceState, DestinationState, DataState, TraceSet>
        where S: Into<String>
    {
        MessageBuilder {
            //Routing
            id: self.id,
            source: self.source,
            destination: self.destination,
            //Payload
            action: self.action,
            data: self.data,
            //Debug
            trace: Some((name.into(), id)),

            source_state: PhantomData,
            destination_state: PhantomData,
            data_state: PhantomData,
            trace_state: PhantomData,
        }
    }

    pub fn data<D>(self,
                   data: &D)
                   -> Result<MessageBuilder<SourceState, DestinationState, DataSet, TraceState>>
        where D: Serialize + ?Sized
    {
        let data = serde_json::to_string(data).map_err(Error::from)?;
        Ok(MessageBuilder {
               //Routing
               id: self.id,
               source: self.source,
               destination: self.destination,
               //Payload
               action: self.action,
               data: Some(data),
               //Debug
               trace: self.trace,

               source_state: PhantomData,
               destination_state: PhantomData,
               data_state: PhantomData,
               trace_state: PhantomData,
           })
    }
}

impl MessageBuilder<SourceSet, DestinationSet, DataSet, TraceSet> {
    pub fn build_packet(self) -> Message {
        Message {
            //Routing
            id: self.id,
            routing: Some(Routing::new(self.source.unwrap(), self.destination.unwrap())),
            msg_type: MessageType::Packet,
            //Payload
            action: self.action,
            data: Ok(self.data.unwrap()),
            stream: None,
            //Debug
            trace: Trace::new(self.trace.unwrap()),
        }
    }
    pub fn build_sync(self) -> Message {
        Message {
            //Routing
            id: self.id,
            routing: Some(Routing::new(self.source.unwrap(), self.destination.unwrap())),
            msg_type: MessageType::Synchronized,
            //Payload
            action: self.action,
            data: Ok(self.data.unwrap()),
            stream: None,
            //Debug
            trace: Trace::new(self.trace.unwrap()),
        }
    }
    pub fn build_stream(self, stream: DataStream) -> Message {
        Message {
            //Routing
            id: self.id,
            routing: Some(Routing::new(self.source.unwrap(), self.destination.unwrap())),
            msg_type: MessageType::Stream,
            //Payload
            action: self.action,
            data: Ok(self.data.unwrap()),
            stream: Some(stream),
            //Debug
            trace: Trace::new(self.trace.unwrap()),
        }
    }
}

#[cfg(test)]
mod test {
    extern crate serde_json;
    use super::*;
    use std::str;
    use uuid;

    lazy_static! {
        static ref MESSAGE: Message = Message { 
            id: uuid::Uuid::nil(),
            routing: Some(Routing {
                source: Signature {device: RoutingType::Any, particle: RoutingType::Any},
                destination: Signature {device: RoutingType::Any, particle: RoutingType::Any},
            }),
            msg_type: MessageType::Packet,
            action: "hello world".into(),
            data: Ok(serde_json::to_string(&vec![0]).unwrap()),
            trace: Trace {
                trace: vec![("Hi".into(), Uuid::nil())]
            },
            stream: None
        };
    }

    #[test]
    fn test_message_build() {
        let mut msg = MessageBuilder::new("hello world")
            .source(Signature::new(RoutingType::Any, RoutingType::Any))
            .destination(Signature::new(RoutingType::Any, RoutingType::Any))
            .data(&vec![0])
            .unwrap()
            .trace("Hi", Uuid::nil())
            .build_packet();

        msg.id = Uuid::nil();
        assert_eq!(msg, *MESSAGE);
    }
}
