use ui::scene::Scene;

pub const STAGE_PREFIX: &'static str = "@redux-scene\\STAGE\\";

pub struct Stage {
    scenes: Vec<String>,
    root: String,
}

impl Stage {
    pub fn new() -> Stage {
        Stage {
            scenes: Vec::new(),
            root: String::new(),
        }
    }
    pub fn reset(&mut self, updates: &mut Vec<String>) {
        self.scenes = Vec::new();
        self.root = String::new();
        updates.push(json!({
            "type": format!("{}{}", STAGE_PREFIX, "RESET")
        })
                             .to_string());
    }
    pub fn add_scene<S>(&mut self, updates: &mut Vec<String>, name: S) -> Scene
        where S: Into<String>
    {
        let name: String = name.into();
        self.scenes.push(name.clone());
        updates.push(json!({
            "type": format!("{}{}", STAGE_PREFIX, "ADD_SCENE"),
            "scene": name.clone()
        })
                             .to_string());
        Scene::new(name.clone())
    }
    pub fn remove_scene<S>(&mut self, updates: &mut Vec<String>, name: S)
        where S: Into<String>
    {
        let name: String = name.into();
        //TODO: self.scenes.remove(name)
        updates.push(json!({
            "type": format!("{}{}", STAGE_PREFIX, "REMOVE_SCENE"),
            "scene": name.clone()
        })
                             .to_string());
    }
    pub fn set_root<S>(&mut self, updates: &mut Vec<String>, root: S)
        where S: Into<String>
    {
        let root: String = root.into();
        self.root = root.clone();
        updates.push(json!({
            "type": format!("{}{}", STAGE_PREFIX, "SET_ROOT"),
            "value": root
        })
                             .to_string());
    }
}
