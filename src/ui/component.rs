use serde_json::{Number, Value};
use std::collections::HashMap;

pub const COMPONENT_PREFIX: &'static str = "@redux-scene\\COMPONENT\\";

#[derive(Clone, Debug)]
pub enum UIPrimitive {
    Bool(bool),
    Number(Number),
    String(String),
}
impl UIPrimitive {
    pub fn to_json(self) -> Value {
        match self {
            UIPrimitive::Bool(b) => Value::Bool(b),
            UIPrimitive::Number(n) => Value::Number(n),
            UIPrimitive::String(s) => Value::String(s),
        }
    }
}

#[derive(Clone, Debug)]
pub enum UIValue {
    Null,
    Bool(bool),
    Number(Number),
    String(String),
    List(Vec<UIPrimitive>),
    Component(Vec<String>),
}

impl UIValue {
    pub fn to_json(self) -> Value {
        match self {
            UIValue::Null => Value::Null,
            UIValue::Bool(b) => Value::Bool(b),
            UIValue::Number(n) => Value::Number(n),
            UIValue::String(s) => Value::String(s),
            UIValue::List(l) => {
                let mut res: Vec<Value> = Vec::new();
                res.push(Value::String("list".into()));
                let mut list: Vec<Value> = Vec::new();
                for item in l {
                    list.push(item.to_json());
                }
                res.push(Value::Array(list));
                Value::Array(res)
            }
            UIValue::Component(c) => {
                let mut res: Vec<Value> = Vec::new();
                res.push(Value::String("component".into()));
                let mut list: Vec<Value> = Vec::new();
                for item in c {
                    list.push(Value::String(item));
                }
                res.push(Value::Array(list));
                Value::Array(res)
            }
        }
    }
}

#[derive(Clone, Debug)]
pub struct UIEvent {
    event: String,
    attr: String,
}
impl UIEvent {
    pub fn new<S, T>(event: S, attr: T) -> UIEvent
        where S: Into<String>,
              T: Into<String>
    {
        UIEvent {
            event: event.into(),
            attr: attr.into(),
        }
    }
    pub fn to_json(self) -> Value {
        json!({
            "type": self.event,
            "attr": self.attr
        })
    }
}

pub struct Component {
    name: String,
    scene: String,
    component_type: String,
    attrs: HashMap<String, UIValue>,
    events: HashMap<String, UIEvent>,
    content: UIValue,
}

impl Component {
    pub fn new<S>(scene: S, name: S) -> Component
        where S: Into<String>
    {
        Component {
            name: name.into(),
            scene: scene.into(),
            component_type: String::new(),
            attrs: HashMap::new(),
            events: HashMap::new(),
            content: UIValue::Null,
        }
    }
    pub fn name(&self) -> String {
        self.name.clone()
    }
    pub fn reset(&mut self, updates: &mut Vec<String>) {
        self.component_type = String::new();
        self.attrs = HashMap::new();
        self.events = HashMap::new();
        self.content = UIValue::Null;
        updates.push(json!({
            "type": format!("{}{}",COMPONENT_PREFIX,"RESET"),
            "scene": self.scene.clone(),
            "component": self.name.clone(),
        })
                             .to_string());
    }

    pub fn set_type<S>(&mut self, updates: &mut Vec<String>, value: S)
        where S: Into<String>
    {
        let value: String = value.into();
        self.component_type = value.clone();
        updates.push(json!({
            "type": format!("{}{}",COMPONENT_PREFIX,"SET_TYPE"),
            "scene": self.scene.clone(),
            "component": self.name.clone(),
            "value": value.clone()
        })
                             .to_string());
    }

    pub fn set_attr<S>(&mut self, updates: &mut Vec<String>, key: S, value: UIValue)
        where S: Into<String>
    {
        let key: String = key.into();
        self.attrs.insert(key.clone(), value.clone());
        updates.push(json!({
            "type": format!("{}{}",COMPONENT_PREFIX,"SET_ATTR"),
            "scene": self.scene.clone(),
            "component": self.name.clone(),
            "key": key,
            "value": value.to_json(),
        })
                             .to_string());
    }

    pub fn set_event<S>(&mut self, updates: &mut Vec<String>, key: S, value: UIEvent)
        where S: Into<String>
    {
        let key: String = key.into();
        self.events.insert(key.clone(), value.clone());
        updates.push(json!({
            "type": format!("{}{}",COMPONENT_PREFIX,"SET_EVENT"),
            "scene": self.scene.clone(),
            "component": self.name.clone(),
            "key": key,
            "value": value.to_json()
        })
                             .to_string());
    }

    pub fn remove_event<S>(&mut self, updates: &mut Vec<String>, key: S) -> Result<UIEvent, String>
        where S: Into<String>
    {
        let key: String = key.into();
        match self.events.remove(&key) {
            Some(rm) => {
                updates.push(json!({
                    "type": format!("{}{}",COMPONENT_PREFIX,"REMOVE_EVENT"),
                    "scene": self.scene.clone(),
                    "component": self.name.clone(),
                    "key": key
                })
                                     .to_string());
                Ok(rm)
            }
            None => Err(format!("Error component.remove_event(): {}.{}.{} does not exist", self.scene.clone(), self.name.clone(), key)),
        }
    }

    pub fn set_content(&mut self, updates: &mut Vec<String>, value: UIValue) {
        self.content = value.clone();
        updates.push(json!({
            "type": format!("{}{}",COMPONENT_PREFIX,"SET_CONTENT"),
            "scene": self.scene.clone(),
            "component": self.name.clone(),
            "value": value.to_json(),
        })
                             .to_string());
    }
}
