use ui::component::Component;

pub const SCENE_PREFIX: &'static str = "@redux-scene\\SCENE\\";

pub struct Scene {
    name: String,
    components: Vec<String>,
    root: Vec<String>,
}

impl Scene {
    pub fn new<S>(name: S) -> Scene
        where S: Into<String>
    {
        Scene {
            name: name.into(),
            components: Vec::new(),
            root: Vec::new(),
        }
    }
    pub fn name(&self) -> String {
        self.name.clone()
    }
    pub fn reset<S>(&mut self, updates: &mut Vec<String>) {
        self.components = Vec::new();
        self.root = Vec::new();
        updates.push(json!({
            "type": format!("{}{}", SCENE_PREFIX, "RESET"),
            "scene": self.name.clone()
        })
                             .to_string());
    }
    pub fn add_component<S>(&mut self, updates: &mut Vec<String>, name: S) -> Component
        where S: Into<String>
    {
        let name: String = name.into();
        self.components.push(name.clone());
        updates.push(json!({
            "type": format!("{}{}", SCENE_PREFIX, "ADD_COMPONENT"),
            "scene": self.name.clone(),
            "component": name.clone()
        })
                             .to_string());
        Component::new(self.name.clone(), name.clone())
    }

    pub fn remove_component<S>(&mut self, updates: &mut Vec<String>, name: S)
        where S: Into<String>
    {
        let name: String = name.into();
        //TODO: self.components.remove(name)
        updates.push(json!({
            "type": format!("{}{}", SCENE_PREFIX, "REMOVE_COMPONENT"),
            "scene": self.name.clone(),
            "component": name.clone()
        })
                             .to_string());
    }
    pub fn set_root(&mut self, updates: &mut Vec<String>, root: Vec<String>) {
        self.root = root.clone();
        updates.push(json!({
            "type": format!("{}{}", SCENE_PREFIX, "SET_ROOT"),
            "scene": self.name.clone(),
            "value": root
        })
                             .to_string());
    }
}
