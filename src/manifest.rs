use crypto;
use serde::{Deserialize, Serialize};
use serde_json;
use std::fs;
use std::io;
use std::io::Read;
use std::net::SocketAddr;
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Hash)]
pub struct Manifest {
    pub atom: AtomManifest,
    pub device: DeviceManifest,
    pub particles: Vec<String>,
    pub system: String,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Hash)]
pub struct AtomManifest {
    pub name: String,
    pub network_id: Uuid
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Hash)]
pub struct DeviceManifest {
    pub id: Uuid,
    pub address: SocketAddr,
    pub sign_key: SignKey,
    pub cipher_key: CipherKey,
}

impl DeviceManifest {
    pub fn new(id: Uuid, address: SocketAddr) -> io::Result<DeviceManifest> {
        Ok(DeviceManifest {
               id: id,
               address: address,
               sign_key: crypto::sign::gen_keypair()?,
               cipher_key: crypto::stream::gen_keypair()?,
           })
    }
    pub fn to_public(self) -> Self {
        DeviceManifest {
            id: self.id,
            address: self.address,
            sign_key: SignKey {
                private: None,
                public: self.sign_key.public,
            },
            cipher_key: CipherKey {
                private: None,
                public: self.cipher_key.public,
            },
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Hash)]
pub struct SignKey {
    pub private: Option<crypto::sign::SecretKey>,
    pub public: crypto::sign::PublicKey,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Hash)]
pub struct CipherKey {
    pub private: Option<crypto::stream::SecretKey>,
    pub public: crypto::stream::PublicKey,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Hash)]
pub struct ParticleManifest {
    pub id: Uuid,
    pub name: String,
    pub permissions: PermissionsManifest,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Hash)]
pub struct PermissionsManifest {
    pub send: Vec<String>,
    pub receive: Vec<String>,
    pub broadcast: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Hash)]
pub struct RedisManifest {
    pub config: String,
    pub socket: String,
    pub db: u32,
}

pub fn read_manifest<M>(path: String) -> M
    where M: Serialize + Deserialize
{
    let mut file: fs::File = fs::File::open(path.clone()).expect(&format!("{}", path));
    let mut data: String = String::new();
    file.read_to_string(&mut data).unwrap();

    serde_json::from_str::<M>(&data).unwrap()
}
