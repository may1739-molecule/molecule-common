use err;
use manifest::CipherKey;
use rand::{OsRng, Rng};
use rust_crypto::chacha20::ChaCha20;
use rust_crypto::curve25519::{curve25519, curve25519_base};
use rust_crypto::symmetriccipher::SynchronousStreamCipher;
use std::io;

#[derive(Eq, PartialEq, Debug, Copy, Clone, Hash, Serialize, Deserialize)]
pub struct PublicKey([u8; 32]);

#[derive(Eq, PartialEq, Debug, Copy, Clone, Hash, Serialize, Deserialize)]
pub struct SecretKey([u8; 32]);

#[derive(Eq, PartialEq, Debug, Copy, Clone, Hash, Serialize, Deserialize)]
pub struct SymmetricKey([u8; 32]);

#[derive(Eq, PartialEq, Debug, Copy, Clone, Hash, Serialize, Deserialize)]
pub struct Nonce([u8; 24]);

#[derive(Copy, Clone)]
pub struct StreamCipher {
    cipher: ChaCha20,
}

impl StreamCipher {
    /*pub fn new(pkey: &PublicKey, skey: &SecretKey, nonce: &Nonce) -> StreamCipher {
        let key = SymmetricKey::new(pkey, skey);
        StreamCipher::new_precomputed(&key, nonce)
    }*/

    pub fn new_precomputed(key: &SymmetricKey, nonce: &Nonce) -> StreamCipher {
        StreamCipher { cipher: ChaCha20::new_xchacha20(key, nonce) }
    }

    pub fn process(&mut self, data: &[u8]) -> Vec<u8> {
        let mut output = vec![0; data.len()];
        self.process_with_buf(data, &mut output);

        output
    }

    pub fn process_with_buf(&mut self, input: &[u8], output: &mut [u8]) {
        self.cipher.process(input, output);
    }
}

impl SecretKey {
    pub fn create_public(&self) -> PublicKey {
        PublicKey(curve25519_base(self))
    }
}

pub fn gen_keypair() -> io::Result<CipherKey> {
    let mut rng = OsRng::new()?;

    let sk = SecretKey(rng.gen());
    let pk = sk.create_public();

    Ok(CipherKey {
           private: Some(sk),
           public: pk,
       })
}

impl SymmetricKey {
    pub fn new(pkey: &PublicKey, skey: &SecretKey) -> SymmetricKey {
        SymmetricKey(curve25519(skey, pkey))
    }
}

impl Nonce {
    pub fn gen() -> err::Result<Nonce> {
        let mut rng = OsRng::new().map_err(err::Error::from)?;

        Ok(Nonce(rng.gen()))
    }
}

tuple_struct_impl!(Nonce, 24);
tuple_struct_impl!(SecretKey, 32);
tuple_struct_impl!(SymmetricKey, 32);
tuple_struct_impl!(PublicKey, 32);

#[cfg(test)]
mod test {
    use super::*;

    const OUR_PK: PublicKey = PublicKey([226, 254, 252, 110, 173, 128, 82, 115, 231, 123, 37, 41,
                                         124, 36, 208, 0, 99, 178, 235, 72, 253, 18, 245, 208,
                                         212, 164, 214, 191, 144, 200, 184, 66]);

    const OUR_SK: SecretKey = SecretKey([50, 37, 164, 49, 3, 245, 220, 164, 98, 163, 45, 150,
                                         201, 90, 166, 14, 93, 187, 144, 132, 136, 251, 239, 88,
                                         147, 1, 221, 252, 202, 57, 86, 109]);

    const THEIR_PK: PublicKey = PublicKey([41, 21, 219, 159, 36, 86, 158, 14, 119, 204, 96, 159,
                                           244, 57, 129, 39, 251, 89, 175, 122, 165, 107, 88,
                                           145, 106, 15, 77, 13, 158, 70, 78, 25]);

    const THEIR_SK: SecretKey = SecretKey([132, 201, 6, 247, 157, 78, 73, 237, 226, 178, 29, 204,
                                           98, 124, 183, 224, 183, 188, 106, 186, 100, 98, 56,
                                           124, 71, 234, 40, 67, 179, 54, 14, 14]);

    const KEY: SymmetricKey = SymmetricKey([43, 132, 123, 153, 93, 173, 195, 111, 84, 109, 225,
                                            234, 36, 110, 146, 62, 175, 63, 84, 73, 4, 24, 230,
                                            23, 243, 231, 157, 144, 38, 167, 122, 7]);

    const PLAINTEXT: &'static [u8] = b"some data";

    #[test]
    fn test_cipher_zero_length_buf() {
        let nonce = Nonce([0; 24]);

        let mut cipher = StreamCipher::new_precomputed(&KEY, &nonce);
        let empty_vec = Vec::new();
        let cipher_text = cipher.process(&empty_vec);
        assert!(cipher_text.is_empty());
    }

    #[test]
    fn test_cipher_zero_length_no_advance() {
        let nonce = Nonce([0; 24]);

        let mut cipher_one = StreamCipher::new_precomputed(&KEY, &nonce);
        let mut cipher_two = StreamCipher::new_precomputed(&KEY, &nonce);

        let empty_vec = Vec::new();
        for _ in 0..100 {
            cipher_one.process(&empty_vec);
        }

        let cipher_text_one = cipher_one.process(PLAINTEXT);
        let cipher_text_two = cipher_two.process(PLAINTEXT);
        assert_eq!(cipher_text_one, cipher_text_two);
    }

    #[test]
    fn test_cipher_decryption() {
        let nonce = Nonce([0; 24]);

        let mut our_cipher = StreamCipher::new_precomputed(&KEY, &nonce);
        let mut their_cipher = StreamCipher::new_precomputed(&KEY, &nonce);

        let ciphertext = our_cipher.process(PLAINTEXT);
        let their_plain = their_cipher.process(&ciphertext);

        assert_eq!(PLAINTEXT, their_plain.as_slice());
    }

    #[test]
    fn test_cipher_multipart_decryption() {
        let nonce = Nonce([0; 24]);

        let mut our_cipher = StreamCipher::new_precomputed(&KEY, &nonce);
        let mut their_cipher = our_cipher;

        let ciphertext = our_cipher.process(PLAINTEXT);
        let mut their_plain_1 = their_cipher.process(&ciphertext[0..5]);
        let mut their_plain_2 = their_cipher.process(&ciphertext[5..9]);
        their_plain_1.append(&mut their_plain_2);

        assert_eq!(PLAINTEXT, their_plain_1.as_slice());
    }

    #[test]
    fn test_cipher_decryption_wrong_key() {
        let bad_key = SymmetricKey([42; 32]);
        let nonce = Nonce([0; 24]);

        let mut our_cipher = StreamCipher::new_precomputed(&KEY, &nonce);
        let mut their_cipher = StreamCipher::new_precomputed(&bad_key, &nonce);

        let ciphertext = our_cipher.process(PLAINTEXT);
        let their_plain = their_cipher.process(&ciphertext);

        assert_ne!(PLAINTEXT, their_plain.as_slice());
    }

    #[test]
    fn test_symmetric_key() {
        let key = SymmetricKey::new(&THEIR_PK, &OUR_SK);
        let key2 = SymmetricKey::new(&OUR_PK, &THEIR_SK);

        assert_eq!(key, key2);
    }

    #[test]
    fn test_public_key_creation() {
        let pkey = OUR_SK.create_public();

        assert_eq!(pkey, OUR_PK);
    }

    #[test]
    fn test_bad_symmetric_key() {
        let key = SymmetricKey::new(&OUR_PK, &OUR_SK);
        let key2 = SymmetricKey::new(&THEIR_PK, &THEIR_SK);

        assert_ne!(key, key2);
    }
}
