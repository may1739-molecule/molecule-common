use manifest::SignKey;
use rand::{OsRng, Rng};
use rust_crypto::ed25519::{keypair, signature, verify};
use std::io;

#[derive(Eq, PartialEq, Debug, Copy, Clone, Hash, Serialize, Deserialize)]
pub struct PublicKey([u8; 32]);

pub struct SecretKey([u8; 64]);
pub struct Signature([u8; 64]);

pub fn gen_keypair() -> io::Result<SignKey> {
    let mut rng = OsRng::new()?;
    let seed: [u8; 32] = rng.gen();

    let (sk, pk) = keypair(&seed);
    Ok(SignKey {
           private: Some(SecretKey(sk)),
           public: PublicKey(pk),
       })
}

impl Signature {
    pub fn sign(message: &[u8], secret_key: &SecretKey) -> Signature {
        Signature(signature(message, secret_key))
    }

    pub fn verify(&self, message: &[u8], public_key: &PublicKey) -> bool {
        verify(message, public_key, &self.0)
    }
}

tuple_struct_impl!(PublicKey, 32);
tuple_struct_impl!(SecretKey, 64);
tuple_struct_impl!(Signature, 64);
large_array_impl!(SecretKey);
large_array_impl!(Signature);

large_array_serde_impl!(SecretKey, 64);
large_array_serde_impl!(Signature, 64);

#[cfg(test)]
mod test {
    extern crate serde_json;
    use super::*;

    const PKEY: PublicKey = PublicKey([234, 96, 11, 195, 20, 75, 108, 167, 95, 246, 71, 226, 193,
                                       196, 84, 191, 180, 159, 87, 132, 15, 209, 59, 237, 51,
                                       120, 47, 228, 97, 122, 98, 133]);

    const SKEY: SecretKey =
        SecretKey([225, 24, 149, 214, 62, 167, 128, 194, 245, 185, 101, 109, 126, 247, 51, 200,
                   133, 121, 75, 122, 179, 164, 237, 243, 168, 75, 202, 169, 29, 166, 25, 86,
                   234, 96, 11, 195, 20, 75, 108, 167, 95, 246, 71, 226, 193, 196, 84, 191, 180,
                   159, 87, 132, 15, 209, 59, 237, 51, 120, 47, 228, 97, 122, 98, 133]);

    const SIG: Signature =
        Signature([154, 90, 158, 49, 96, 238, 243, 229, 210, 140, 25, 154, 131, 168, 227, 85,
                   242, 28, 220, 16, 16, 111, 135, 210, 27, 120, 223, 196, 64, 241, 109, 90, 102,
                   255, 61, 179, 68, 223, 144, 62, 89, 114, 180, 157, 70, 48, 188, 176, 99, 32,
                   40, 20, 161, 189, 145, 30, 179, 140, 164, 86, 122, 122, 234, 3]);

    const MESSAGE: &'static str = "Test signature message";

    const SKEY_SER: &'static str = "[225,24,149,214,62,167,128,194,245,185,101,109,126,247,51,200,\
                                    133,121,75,122,179,164,237,243,168,75,202,169,29,166,25,86,\
                                    234,96,11,195,20,75,108,167,95,246,71,226,193,196,84,191,180,\
                                    159,87,132,15,209,59,237,51,120,47,228,97,122,98,133]";

    const SIG_SER: &'static str = "[154,90,158,49,96,238,243,229,210,140,25,154,131,168,227,85,\
                                   242,28,220,16,16,111,135,210,27,120,223,196,64,241,109,90,102,\
                                   255,61,179,68,223,144,62,89,114,180,157,70,48,188,176,99,32,40,\
                                   20,161,189,145,30,179,140,164,86,122,122,234,3]";
    #[test]
    fn test_sign_message() {
        let sig = Signature::sign(MESSAGE.as_bytes(), &SKEY);
        assert_eq!(SIG, sig);
    }

    #[test]
    fn test_verify_signature() {
        SIG.verify(MESSAGE.as_bytes(), &PKEY);
    }

    #[test]
    fn test_secret_serialize() {
        let s = serde_json::to_string(&SKEY).unwrap();
        assert_eq!(SKEY_SER, s);
    }

    #[test]
    fn test_secret_deserialize() {
        let s = serde_json::from_str(SKEY_SER).unwrap();
        assert_eq!(SKEY, s);
    }

    #[test]
    fn test_signature_serialize() {
        let s = serde_json::to_string(&SIG).unwrap();
        assert_eq!(SIG_SER, s);
    }

    #[test]
    fn test_signature_deserialize() {
        let s = serde_json::from_str(SIG_SER).unwrap();
        assert_eq!(SIG, s);
    }
}
