#[macro_export]
macro_rules! tuple_struct_impl {
    ($t:ident, $length:expr) => {
        impl ::std::ops::Deref for $t {
            type Target = [u8];

            fn deref(&self) -> &Self::Target {
                &self.0
            }
        }

        impl ::std::ops::DerefMut for $t {
            fn deref_mut(&mut self) -> &mut Self::Target {
                &mut self.0
            }
        }

        impl ::std::convert::AsRef<[u8]> for $t {
            fn as_ref(&self) -> &[u8] {
                &self.0
            }
        }

        impl $t {
            #[allow(dead_code)]
            pub fn from_bytes(bytes: &[u8]) -> ::err::Result<$t> {
                let len = bytes.len();
                if len < $length {
                    Err(::err::Error::new(::err::ErrKind::IO, "buffer too short"))
                } else {
                    let mut output = $t([0u8; $length]);
                    for (slot, val) in output.iter_mut().zip(bytes.iter()) {
                        *slot = *val;
                    }

                    Ok(output)
                }
            }
        }
    }
}

macro_rules! large_array_impl {
    ($t:ty) => {
        impl Copy for $t { }

        impl Clone for $t {
            fn clone(&self) -> $t {
                *self
            }
        }

        impl ::std::hash::Hash for $t {
            fn hash<H>(&self, state: &mut H) where H: ::std::hash::Hasher {
                self.0.hash(state)
            }
        }

        impl PartialEq for $t {
            fn eq(&self, other: &$t) -> bool {
                self.0[..] == other.0[..]
            }
        }

        impl Eq for $t {}
        
        impl ::std::fmt::Debug for $t {
            fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
                self.0.fmt(f)
            }
        }
    }
}
macro_rules! large_array_serde_impl {
    ($t:ident, $s:expr) => {
        impl ::serde::Serialize for $t {
            fn serialize<S>(&self, serializer: S) -> ::std::result::Result<S::Ok, S::Error>
                where S: ::serde::Serializer 
            {
                self.0.serialize(serializer)
            }
        }
        
        impl ::serde::Deserialize for $t {
            fn deserialize<D>(deserializer: D) -> ::std::result::Result<Self, D::Error>
                where D: ::serde::Deserializer
            {
                struct DeVisitor;

                impl ::serde::de::Visitor for DeVisitor {
                    type Value = $t;

                    fn expecting(&self, formatter: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
                        write!(formatter, "a sequence of integers")
                    }
                    
                    fn visit_seq<V>(self, mut visitor: V) -> ::std::result::Result<$t, V::Error>
                        where V: ::serde::de::SeqVisitor
                    {
                        let mut value = $t([0; $s]);

                        for i in 0..$s {
                            value.0[i] = match try!(visitor.visit()) {
                                Some(val) => val,
                                None => { return Err(::serde::de::Error::invalid_length($s, &self)); }
                            };
                        }

                        Ok(value)
                    }
                }

                deserializer.deserialize_seq_fixed_size($s, DeVisitor)
            }
        }
    }
}

macro_rules! log_target {
    ( $t:expr ) => {
        &concat!(module_path!(), "::", $t)
    }
}

macro_rules! state_root {
    () => ( "root" )
}

macro_rules! eif {
    ($map:expr) => (
        |e| {
            error!("Error In Flight: {:?}", e);
            $map(e)
        }
    )
}


macro_rules! rif {
    ($f:expr) => {
        |res| {
            match res {
                Ok(ref o) => info!("{:?}", o),
                Err(ref e) => error!("{:?}", e),
            }
            $f(res)
        }
    }
}

macro_rules! mif {
    ($f:expr) => {
        |res| {
            match res {
                Ok(ref o) => info!("\r\n{:?}", o.get_raw()),
                Err(ref e) => error!("{:?}", e),
            }
            $f(res)
        }
    }
}

macro_rules! result {
    ($level:expr, $val:expr, $ok:expr, $err:expr) => (
        match &$val {
            &::std::result::Result::Ok(_) => log!($level, $ok),
            &::std::result::Result::Err(ref e) => error!("{:?}", e),
        }
    )
}

macro_rules! ftry {
    ($v:expr) => (
        match $v {
            Err(e) => return future::err(e.into()).boxed(),
            Ok(a) => a,
        }
    )
}