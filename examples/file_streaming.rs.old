extern crate tokio_core;
extern crate futures;
extern crate tokio_service;
extern crate molecule_common;
extern crate bincode;
extern crate uuid;


use std::io::{self, Read, Write};
use std::path::Path;
use std::thread;

use bincode::SizeLimit;
use futures::{Future, future, IntoFuture, Sink, Stream};
use tokio_core::reactor::Core;
use tokio_service::Service;
use uuid::Uuid;

use molecule_common::Particle;
use molecule_common::message::{self, RequestMsg, HeaderBuilder, DataStream, RoutingType};
use molecule_common::{DeviceDB, HashDeviceDB};
use molecule_common::networking::{CMBackend, LanBackend};

fn main() {
    let mut core = Core::new().unwrap();
    let handle = core.handle();

    let addr = "127.0.0.1:8080".parse().unwrap();

    let server_local = molecule_common::LocalDevice::new(addr).unwrap();

    let client_local = molecule_common::LocalDevice::new(addr).unwrap();

    let particle = Particle {
        id: Uuid::nil(),
        name: "Files".into(),
        actions: vec![],
    };

    let server_db = HashNetDB::new(HashDeviceDB::with_local(server_local));
    server_db.add_device(client_local.into());
    server_db.set_addr(&client_local.into(), addr);

    let client_db = HashNetDB::new(HashDeviceDB::with_local(client_local));
    client_db.add_device(server_local.into());
    client_db.set_addr(&server_local.into(), addr);
    client_db.add_particle(server_local.into(), particle.clone());


    // server backend
    LanBackend::new(server_db, &handle, addr).serve(|| Ok(FileService));

    // client backend
    let client = LanBackend::new(client_db, &handle, addr);
    let (mut tx, rx) = futures::sync::mpsc::unbounded();

    thread::spawn(move || loop {
        let mut s = String::new();
        std::io::stdin().read_line(&mut s).unwrap();
        s = s.trim().into();
        let data: String = match &*s {
            "list" => "".into(),
            "get" => {
                let mut file_name = String::new();
                std::io::stdin().read_line(&mut file_name).unwrap();
                file_name.trim().into()
            }
            _ => {
                println!("Bad command: {:?}", s);
                continue;
            }
        };
        let sig = molecule_common::message::Signature::new(RoutingType::Any,
                                                           RoutingType::One(Uuid::nil()));
        let message = RequestMsg::Packet(HeaderBuilder::new(s)
            .destination(sig.clone())
            .source(sig)
            .data(&data)
            .unwrap()
            .build());

        tx = tx.send(message).wait().unwrap();
    });

    let cli = rx.map_err(|_| io::Error::new(io::ErrorKind::BrokenPipe, "oops"))
        .and_then(|command| client.request(command))
        .for_each(|response| match response.get_action() {
            "list" => {
                let files: Vec<String> = response.get_data().unwrap();

                for file in files {
                    println!("{}", file);
                }
                future::ok(()).boxed()
            }
            "get" => {
                match response {
                    RequestMsg::Stream(req_header, stream) => {
                        let file_name: String = req_header.get_data().unwrap();
                        let mut file = std::fs::File::create(Path::new("examples/client")
                                .join(file_name))
                            .unwrap();
                        stream.for_each(move |vec| {
                                file.write_all(&vec).unwrap();
                                Ok(())
                            })
                            .boxed()
                    }
                    _ => {
                        println!("Operation failed");
                        future::ok(()).boxed()
                    }
                }
            }
            _ => {
                println!("Server sent bad response");
                future::ok(()).boxed()
            }
        });

    let resolved = core.run(cli);
    println!("{:?}", resolved);
}

struct FileService;
impl Service for FileService {
    type Request = RequestMsg;
    type Response = RequestMsg;
    type Error = io::Error;
    type Future = Box<Future<Item = RequestMsg, Error = io::Error>>;

    fn call(&self, req: Self::Request) -> Self::Future {
        println!("{}", req);
        let result = match req.get_action() {
            "list" => {
                let files: Vec<_> = std::fs::read_dir("./examples/server")
                    .unwrap()
                    .filter_map(|entry| entry.ok())
                    .map(|entry| entry.file_name().into_string().unwrap())
                    .collect();
                let data = bincode::serialize(&files, SizeLimit::Infinite).unwrap();
                let response = message::ResponseMsg::Packet(data);
                Ok(RequestMsg::from(response))
            }
            "get" => {
                let file_name: String = req.get_data().unwrap();
                let path = Path::new("./examples/server").join(file_name);
                println!("finding file: {:?}", path);

                let (mut sink, stream) = DataStream::pair();
                match std::fs::File::open(path) {
                    Ok(mut file) => {
                        println!("found file");
                        thread::spawn(move || {
                            let mut bytes = [0u8; 2048];
                            loop {
                                let n = file.read(&mut bytes).unwrap();
                                if n == 0 {
                                    break;
                                }
                                let v = Vec::from(&bytes[0..n]);
                                sink = sink.send(Ok(v)).wait().expect("couldnt put data into sink");
                            }
                        });
                        Ok(RequestMsg::from(message::ResponseMsg::Stream(req.get_raw().clone(), stream)))
                    }
                    Err(_) => {
                        let vec = bincode::serialize("file does not exist", SizeLimit::Infinite)
                            .unwrap();
                        Ok(RequestMsg::from(message::ResponseMsg::Packet(vec)))
                    }
                }
            }
            _ => Err(io::Error::new(io::ErrorKind::InvalidInput, "Not a valid command")),
        };

        Box::new(result.into_future())
    }
}
