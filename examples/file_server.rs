extern crate tokio_core;
extern crate molecule_common;
extern crate uuid;
extern crate futures;
#[macro_use]
extern crate serde_derive;
use futures::{BoxFuture, Future, Sink, Stream, future};
use molecule_common::app;
use molecule_common::err::*;
use molecule_common::message::{DataStream, Message};
use molecule_common::reqes::*;
use std::io::Read;
use std::path::Path;
use std::thread;
use tokio_core::reactor::Core;
use uuid::Uuid;

#[derive(Serialize, Deserialize)]
pub enum FileAction {
    List,
    Get(String),
}

fn main() {
    let mut core = Core::new().unwrap();
    let reqes: Reqes<Message> = app::start(core.handle()).unwrap();

    let reqes_server = reqes.server;
    let handle = core.handle();
    let in_server = reqes_server.receive()
        .and_then(|mut req| -> BoxFuture<(), Error> {
            let msg: Message = req.take_data().unwrap();
            let action: Result<FileAction> = msg.get_data();
            let result: Box<Future<Item = Message, Error = Error>> = match action {
                Ok(FileAction::List) => {
                    let files: Vec<_> = std::fs::read_dir("./data")
                        .unwrap()
                        .filter_map(|entry| entry.ok())
                        .map(|entry| entry.file_name().into_string().unwrap())
                        .collect();
                    let response = msg.response()
                        .data(Ok(files))
                        .trace("File Server", Uuid::nil())
                        .build_packet();
                    Box::new(future::ok(response))
                }
                Ok(FileAction::Get(file_name)) => {
                    let path = Path::new("./data").join(file_name);
                    println!("finding file: {:?}", path);

                    let (mut sink, stream) = DataStream::pair();
                    match std::fs::File::open(path) {
                        Ok(mut file) => {
                            println!("found file");
                            thread::spawn(move || {
                                let mut bytes = [0u8; 2048];
                                loop {
                                    let n = file.read(&mut bytes).unwrap();
                                    if n == 0 {
                                        break;
                                    }
                                    let v = Vec::from(&bytes[0..n]);
                                    sink = sink.send(Ok(v))
                                        .wait()
                                        .expect("couldnt put data into sink");
                                }
                            });
                            let data = msg.get_raw().clone();
                            let response = msg.response()
                                .data(data.clone())
                                .trace("File Server", Uuid::nil())
                                .build_stream(stream);
                            Box::new(future::ok(response))
                        }
                        Err(_) => {
                            let response = msg.response()
                                .data::<()>(Err(Error::new(ErrKind::Other, "file does not exist")))
                                .trace("File Server", Uuid::nil())
                                .build_packet();
                            Box::new(future::ok(response))
                        }
                    }
                }
                _ => {
                    let response = msg.response()
                        .data::<()>(Err(Error::new(ErrKind::Other, "Not a valid command")))
                        .trace("File Server", Uuid::nil())
                        .build_packet();
                    Box::new(future::ok(response))
                }
            };
            req.reply(&handle, result);
            future::ok(()).boxed()
        })
        .map_err(|_| ())
        .for_each(|_| -> BoxFuture<(), ()> { future::ok(()).boxed() });


    core.run(in_server).unwrap();
}
