extern crate futures;
extern crate futures_cpupool;
extern crate molecule_common;
extern crate r2d2;
extern crate r2d2_redis;
extern crate redis;
extern crate serde;

use futures::Future;
use molecule_common::err;
use molecule_common::manifest::RedisManifest;
use molecule_common::state::State;
use molecule_common::state::record::*;
use molecule_common::state::action::*;
use molecule_common::state::redis::*;

fn main() {

    let db = RedisClientPool::new(&RedisManifest {
                                       config: "/tmp/project_molecule/redis.conf".into(),
                                       socket: "/tmp/project_molecule/redis.sock".into(),
                                       db: 10,
                                   })
            .unwrap();
    let my_state = State::new(db);
    let mut actions: Vec<StateAction> = Vec::new();
    actions.push(StateAction::create("key1", &1).unwrap());
    actions.push(StateAction::create("key2", &2).unwrap());
    actions.push(StateAction::create("key1.key3", &3).unwrap());
    actions.push(StateAction::create("key1.key4", &1).unwrap());
    my_state.injest(StateAction::from(actions))
        .and_then(|rec| {
            print_result(rec);
            my_state.injest(StateAction::read("root"))
        })
        .and_then(|rec| {
            print_result(rec);
            my_state.injest(StateAction::delete("key1"))
        })
        //.and_then(|_| my_state.injest(StateAction::delete("root")))
        .and_then(|rec| {
            print_result(rec);
            my_state.injest(StateAction::read("root"))
        })
        .map_err(print_error)
        .wait()
        .unwrap();
    println!("{:?}", my_state);
}

fn print_result(res: Transaction) -> () {
    println!("{:?}", res);
    ()
}

fn print_error(err: err::Error) {
    println!("{:?}", err);
    ()
}
